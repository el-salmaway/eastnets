package com.en.common.enums;

public enum DuplicateTypesEnum implements EnumClass<Integer>{
	FULL_DUPLICATE(1),PARTIAL_DUPLICATE(2);
	private	Integer id;
	
	
	DuplicateTypesEnum(Integer value) {
		this.id = value;
	}
	
	
 	public DuplicateTypesEnum fromId(Integer id) {
		for (DuplicateTypesEnum at : DuplicateTypesEnum.values()) {
			if (at.getId().equals(id)) {
				return at;
			}
		}
		return null;
	}


	@Override
	public Integer getId() {
		// TODO Auto-generated method stub
		return this.id;
	}
	
	//Override method
		 public boolean equalsName(Integer id) {
		        return id.equals(this.id);
		    }
	 
}
