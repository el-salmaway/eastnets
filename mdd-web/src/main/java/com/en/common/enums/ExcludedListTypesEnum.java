package com.en.common.enums;

public enum ExcludedListTypesEnum implements EnumClass<String> {
	EXCLUDED_LOG_TERM("excludedLogicalTerminal"), EXCLUDED_REFERNCES("excludedReferences");

	private String id;

	ExcludedListTypesEnum(String value) {
		this.id = value;
	}

	public ExcludedListTypesEnum fromId(String id) {
		for (ExcludedListTypesEnum at : ExcludedListTypesEnum.values()) {
			if (at.getId().equals(id)) {
				return at;
			}
		}
		return null;
	}

	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return this.id;
	}

	// Override method
	public boolean equalsName(String id) {
		return id.equals(this.id);
	}

}
