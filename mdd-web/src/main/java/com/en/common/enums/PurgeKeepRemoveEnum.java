package com.en.common.enums;

public enum PurgeKeepRemoveEnum implements EnumClass<Integer>{
	PURGE_KEEP(1),PURGE_REMOVE(2);
	private	Integer id;
	
	
	PurgeKeepRemoveEnum(Integer value) {
		this.id = value;
	}
	
	
 	public PurgeKeepRemoveEnum fromId(String id) {
		for (PurgeKeepRemoveEnum at : PurgeKeepRemoveEnum.values()) {
			if (at.getId().equals(id)) {
				return at;
			}
		}
		return null;
	}


	@Override
	public Integer getId() {
		// TODO Auto-generated method stub
		return this.id;
	}
	
	//Override method
		 public boolean equalsName(Integer id) {
		        return id.equals(this.id);
		    }
	 
}
