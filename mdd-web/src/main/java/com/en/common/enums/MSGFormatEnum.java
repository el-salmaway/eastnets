package com.en.common.enums;
 
public enum MSGFormatEnum implements EnumClass<Integer>{
	FIN(1),MX(2);
	private	Integer id;
	
	
	MSGFormatEnum(Integer value) {
		this.id = value;
	}
	
	
 	public MSGFormatEnum fromId(String id) {
		for (MSGFormatEnum at : MSGFormatEnum.values()) {
			if (at.getId().equals(id)) {
				return at;
			}
		}
		return null;
	}


	@Override
	public Integer getId() {
		// TODO Auto-generated method stub
		return this.id;
	}
	
	//Override method
		 public boolean equalsName(Integer id) {
		        return id.equals(this.id);
		    }
	 
}
