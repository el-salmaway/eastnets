package com.en.common.enums;



public enum UUMIDFiledsEnum implements EnumClass<Integer>{
	
	
	CORRESPONDENT_BIC(1),
	SENDER(2),
	REQUESTOR(3),
	RECEIVER(4),
	RESPONDER(5),
	MESSAGE_TYPE(6),
	REFERENCE(7),
	REQUEST_TYPE(8),
	UNIQUE_MESSAGE_IDENTIFIER(9),
	DIRECTION(10);

	
	UUMIDFiledsEnum(Integer value) {
			this.id = value;
		}

		private Integer id;

		public Integer getId() {
			return id;
		}

		public static UUMIDFiledsEnum fromId(String id) {
			for (UUMIDFiledsEnum at : UUMIDFiledsEnum.values()) {
				if (at.getId().equals(id)) {
					return at;
				}
			}
			return null;
		}

}
