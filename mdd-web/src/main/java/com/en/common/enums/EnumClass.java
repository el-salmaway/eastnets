package com.en.common.enums;
public interface EnumClass<T> {
    T getId();
}