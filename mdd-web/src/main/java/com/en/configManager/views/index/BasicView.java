package com.en.configManager.views.index;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.en.configManager.beans.Configuration;
import com.en.configManager.service.ConfigurationService;
 
@ManagedBean(name="dtBasicView")
@ViewScoped
public class BasicView extends SpringBeanAutowiringSupport  implements Serializable {

   
	private static final long serialVersionUID = 1L;

	private List<Configuration> configurations;
	private Configuration selectedConfiguration;
	private List<Configuration> selectedConfigurations;
	private List<Configuration> filteredConfigurations;
 
	@Autowired	
    private ConfigurationService service;

    @PostConstruct
    public void init() {
    	
	 
    	configurations = service.getConfigurations();
    	filteredConfigurations=service.getConfigurations();
    }

    public List<Configuration> getConfigurations() {
        return configurations;
    }

    public void setService(ConfigurationService service) {
        this.service = service;
    }

	public Configuration getSelectedConfiguration() {
		return selectedConfiguration;
	}

	public void setSelectedConfiguration(Configuration selectedConfiguration) {
		this.selectedConfiguration = selectedConfiguration;
	}

	public List<Configuration> getSelectedConfigurations() {
		return selectedConfigurations;
	}

	public void setSelectedConfigurations(List<Configuration> selectedConfigurations) {
		this.selectedConfigurations = selectedConfigurations;
	}

	public ConfigurationService getService() {
		return service;
	}

	public void setConfigurations(List<Configuration> configurations) {
		this.configurations = configurations;
	}

	public List<Configuration> getFilteredConfigurations() {
		return filteredConfigurations;
	}

	public void setFilteredConfigurations(List<Configuration> filteredConfigurations) {
		this.filteredConfigurations = filteredConfigurations;
	}
    
    
    
    
    
}