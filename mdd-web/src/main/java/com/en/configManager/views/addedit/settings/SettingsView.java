package com.en.configManager.views.addedit.settings;

import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean(name="settingsView")
@RequestScoped
public class SettingsView {
	
	private Integer retentionPeriod;
	private boolean checkdupPDEOutgoing;
	private boolean checkdupPDEIncoming;
	private boolean checkRetrieved;
	private Date purgeTime;
	private Date lastPurgTime;
	private Date nextPurgeTime;
	private boolean enableRecovery;

	
    private String purgeKeepOrREmove;

	public Integer getRetentionPeriod() {
		return retentionPeriod;
	}

	public void setRetentionPeriod(Integer retentionPeriod) {
		this.retentionPeriod = retentionPeriod;
	}

	public boolean isCheckdupPDEIncoming() {
		return checkdupPDEIncoming;
	}

	public void setCheckdupPDEIncoming(boolean checkdupPDEIncoming) {
		this.checkdupPDEIncoming = checkdupPDEIncoming;
	}

	public boolean isCheckdupPDEOutgoing() {
		return checkdupPDEOutgoing;
	}

	public void setCheckdupPDEOutgoing(boolean checkdupPDEOutgoing) {
		this.checkdupPDEOutgoing = checkdupPDEOutgoing;
	}

	public boolean isCheckRetrieved() {
		return checkRetrieved;
	}

	public void setCheckRetrieved(boolean checkRetrieved) {
		this.checkRetrieved = checkRetrieved;
	}

	public Date getPurgeTime() {
		return purgeTime;
	}

	public void setPurgeTime(Date purgeTime) {
		this.purgeTime = purgeTime;
	}

	public String getPurgeKeepOrREmove() {
		return purgeKeepOrREmove;
	}

	public void setPurgeKeepOrREmove(String purgeKeepOrREmove) {
		this.purgeKeepOrREmove = purgeKeepOrREmove;
	}

	public Date getNextPurgeTime() {
		return nextPurgeTime;
	}

	public void setNextPurgeTime(Date nextPurgeTime) {
		this.nextPurgeTime = nextPurgeTime;
	}

	public Date getLastPurgTime() {
		return lastPurgTime;
	}

	public void setLastPurgTime(Date lastPurgTime) {
		this.lastPurgTime = lastPurgTime;
	}

	public boolean isEnableRecovery() {
		return enableRecovery;
	}

	public void setEnableRecovery(boolean enableRecovery) {
		this.enableRecovery = enableRecovery;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	

}
