package com.en.configManager.views.addedit.configuration;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import javax.faces.bean.ManagedProperty;

import org.primefaces.model.TreeNode;

import com.en.common.enums.DuplicateTypesEnum;
import com.en.common.enums.MSGFormatEnum;
import com.en.config.resourceBundle.MessageProvider;
import com.en.configManager.dao.entities.SyntaxMessage;
import com.en.configManager.dao.entities.SyntaxVersion;
import com.en.configManager.service.ConfigurationService;

public class ConfigurationView {
    @ManagedProperty("#{msgBundle}")
    private MessageProvider msg;

    private Map<String, Integer> msgFormates;
    private Map<String, Long> syntaxVersions;
    private Map<String, Long> finMsgTypes;
    private String selectedMsgFormat;
    private String selectedSyntaxVersion;
    private String selectedFinMsgType;
    private DuplicateTypesEnum duplicateType;
    private ConfigurationService service;
    private TreeNode configFieldsRoot;
    private TreeNode[] selectedNodes;


    public ConfigurationView(ConfigurationService service) {
        this.service = service;
        service.test();

        msgFormates = new HashMap<String, Integer>();
        for (MSGFormatEnum formatEnum : MSGFormatEnum.values()) {
            msgFormates.put(formatEnum.name(), formatEnum.getId());

        }

        setFinMsgTypes(service.getAllFinMSGTypes().stream().collect(Collectors.toMap(SyntaxMessage::getType, SyntaxMessage::getIdx)));

        syntaxVersions = service.getAllSyntaxVersions().stream().collect(Collectors.toMap(SyntaxVersion::getVersion, SyntaxVersion::getIdx));


        this.configFieldsRoot = service.test();


    }


    public Map<String, Integer> getMsgFormates() {
        return msgFormates;
    }


    public void setMsgFormates(Map<String, Integer> msgFormates) {
        this.msgFormates = msgFormates;
    }


    public Map<String, Long> getSyntaxVersions() {
        return syntaxVersions;
    }


    public void setSyntaxVersions(Map<String, Long> syntaxVersions) {
        this.syntaxVersions = syntaxVersions;
    }


    public String getSelectedMsgFormat() {
        return selectedMsgFormat;
    }


    public void setSelectedMsgFormat(String selectedMsgFormat) {
        this.selectedMsgFormat = selectedMsgFormat;
    }


    public String getSelectedSyntaxVersion() {
        return selectedSyntaxVersion;
    }


    public void setSelectedSyntaxVersion(String selectedSyntaxVersion) {
        this.selectedSyntaxVersion = selectedSyntaxVersion;
    }


    public String getSelectedFinMsgType() {
        return selectedFinMsgType;
    }


    public void setSelectedFinMsgType(String selectedFinMsgType) {
        this.selectedFinMsgType = selectedFinMsgType;
    }


    public ConfigurationService getService() {
        return service;
    }


    public void setService(ConfigurationService service) {
        this.service = service;
    }


    public Map<String, Long> getFinMsgTypes() {
        return finMsgTypes;
    }


    public void setFinMsgTypes(Map<String, Long> finMsgTypes) {
        this.finMsgTypes = finMsgTypes;
    }


    public Integer getDuplicateType() {
        return (duplicateType != null) ? duplicateType.getId() : null;
    }


    public void setDuplicateType(Integer duplicateType) {
        this.duplicateType = DuplicateTypesEnum.FULL_DUPLICATE.fromId(duplicateType);
    }


    public TreeNode getConfigFieldsRoot() {
        return configFieldsRoot;
    }


    public void setConfigFieldsRoot(TreeNode configFieldsRoot) {
        this.configFieldsRoot = configFieldsRoot;
    }


    public TreeNode[] getSelectedNodes() {
        return selectedNodes;
    }


    public void setSelectedNodes(TreeNode[] selectedNodes) {
        this.selectedNodes = selectedNodes;
    }


}
