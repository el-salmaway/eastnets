package com.en.configManager.views.addedit.excludedList;

import java.util.List;

import java.util.Optional;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import org.primefaces.component.orderlist.OrderList;

import com.en.configManager.beans.ExcludedList;




@ManagedBean(name = "excludedListConvertor")
@FacesConverter(value = "excludedListConvertor")
@RequestScoped
public class ExcludedListConvertor implements Converter {

   
	@Override
	public ExcludedList getAsObject(FacesContext context, UIComponent component, String value) {
		List<ExcludedList> items=   (List)(((OrderList)component).getValue());
		
		if (value != null && value.trim().length() > 0) {
            try {
            	Optional<ExcludedList>item= items.stream().filter(e-> e.getId()==Long.parseLong(value)).findAny();
            	           			
                return (item.isPresent())?item.get():null;
            } catch (NumberFormatException e) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid country."));
            }
        } else {
            return null;
        }
	}

	 

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
            return String.valueOf(((ExcludedList)value).getId());
        } else {
            return null;
        }
	}
}