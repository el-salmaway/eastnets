package com.en.configManager.views.addedit.excludedList;

import java.util.ArrayList;
import java.util.List;

import com.en.configManager.beans.ExcludedList;

public class ExcludedListView {
	
	private List<ExcludedList>excludedLogicalTerminals;
	private List<ExcludedList>excludedReferences;
	
	
	
	public ExcludedListView() {
		
		excludedLogicalTerminals=new ArrayList<ExcludedList>();
		excludedReferences=new ArrayList<ExcludedList>();

		
		
	}
	
	
	
	
	
	
	
	
	
	public List<ExcludedList> getExcludedLogicalTerminals() {
		return excludedLogicalTerminals;
	}
	public void setExcludedLogicalTerminals(List<ExcludedList> excludedLogicalTerminals) {
		this.excludedLogicalTerminals = excludedLogicalTerminals;
	}
	public List<ExcludedList> getExcludedReferences() {
		return excludedReferences;
	}
	public void setExcludedReferences(List<ExcludedList> excludedReferences) {
		this.excludedReferences = excludedReferences;
	}

	
	
	

}
