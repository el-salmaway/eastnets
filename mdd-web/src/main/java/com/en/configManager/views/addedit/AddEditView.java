package com.en.configManager.views.addedit;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.en.configManager.beans.Configuration;
import com.en.configManager.service.ConfigurationService;
import com.en.configManager.views.addedit.configuration.ConfigurationView;
import com.en.configManager.views.addedit.excludedList.ExcludedListView;
import com.en.configManager.views.addedit.mailNotification.MailNotification;
import com.en.configManager.views.addedit.preCondition.PreConditionView;
import com.en.configManager.views.addedit.settings.SettingsView;
 
@ManagedBean(name="addEditView")
@ViewScoped
public class AddEditView extends SpringBeanAutowiringSupport implements Serializable {

   
	private static final long serialVersionUID = 1L;

	private List<Configuration> configurations;
	private Configuration selectedConfiguration;
	private List<Configuration> selectedConfigurations;
	private List<Configuration> filteredConfigurations;
	
	private SettingsView settingsView;
	private PreConditionView preConditionView;
	private MailNotification mailNotification;
	private ConfigurationView configurationView ;
	private ExcludedListView excludedListView;
	@Autowired
    private ConfigurationService service;

    @PostConstruct
    public void init() {
    	
    	settingsView	= new SettingsView();
    	preConditionView= new PreConditionView(service);
    	mailNotification= new MailNotification(service);
    	configurationView=new ConfigurationView(service);
    	excludedListView=new ExcludedListView();
    	configurations = service.getConfigurations();
    	filteredConfigurations=service.getConfigurations();
    }

    public List<Configuration> getConfigurations() {
        return configurations;
    }

    public void setService(ConfigurationService service) {
        this.service = service;
    }

	public Configuration getSelectedConfiguration() {
		return selectedConfiguration;
	}

	public void setSelectedConfiguration(Configuration selectedConfiguration) {
		this.selectedConfiguration = selectedConfiguration;
	}

	public List<Configuration> getSelectedConfigurations() {
		return selectedConfigurations;
	}

	public void setSelectedConfigurations(List<Configuration> selectedConfigurations) {
		this.selectedConfigurations = selectedConfigurations;
	}

	public ConfigurationService getService() {
		return service;
	}

	public void setConfigurations(List<Configuration> configurations) {
		this.configurations = configurations;
	}

	public List<Configuration> getFilteredConfigurations() {
		return filteredConfigurations;
	}

	public void setFilteredConfigurations(List<Configuration> filteredConfigurations) {
		this.filteredConfigurations = filteredConfigurations;
	}

	public SettingsView getSettingsView() {
		return settingsView;
	}

	public void setSettingsView(SettingsView settingsView) {
		this.settingsView = settingsView;
	}

	public PreConditionView getPreConditionView() {
		return preConditionView;
	}

	public void setPreConditionView(PreConditionView preConditionView) {
		this.preConditionView = preConditionView;
	}

	public MailNotification getMailNotification() {
		return mailNotification;
	}

	public void setMailNotification(MailNotification mailNotification) {
		this.mailNotification = mailNotification;
	}

	public ConfigurationView getConfigurationView() {
		return configurationView;
	}

	public void setConfigurationView(ConfigurationView configurationView) {
		this.configurationView = configurationView;
	}

	public ExcludedListView getExcludedListView() {
		return excludedListView;
	}

	public void setExcludedListView(ExcludedListView excludedListView) {
		this.excludedListView = excludedListView;
	}
    
    
    
    
    
}