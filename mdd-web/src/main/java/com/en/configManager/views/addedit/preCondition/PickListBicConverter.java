package com.en.configManager.views.addedit.preCondition;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.primefaces.component.picklist.PickList;
import org.primefaces.model.DualListModel;

import com.en.configManager.beans.Bic;

@ManagedBean(name = "bicConverter")
@FacesConverter(value = "bicConverter")
@RequestScoped
public class PickListBicConverter implements Converter {
	@Override
	public Object getAsObject(FacesContext fc, UIComponent comp, String value) {
		DualListModel<Bic> model = (DualListModel<Bic>) ((PickList) comp).getValue();
		for (Bic bic : model.getSource()) {
			if (bic.getId().equals(value)) {
				return bic;
			}
		}
		for (Bic bic : model.getTarget()) {
			if (bic.getId().equals(value)) {
				return bic;
			}
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext fc, UIComponent comp, Object value) {
		return ((Bic) value).getId().toString();
	}
}