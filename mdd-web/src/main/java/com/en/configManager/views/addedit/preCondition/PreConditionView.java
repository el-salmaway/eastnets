package com.en.configManager.views.addedit.preCondition;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.event.SelectEvent;
import org.primefaces.event.TransferEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.DualListModel;

import com.en.configManager.beans.Bic;
import com.en.configManager.beans.Configuration;
import com.en.configManager.service.ConfigurationService;

public class PreConditionView {

	private ConfigurationService service;

	private List<Configuration> configurations;

	private DualListModel<Bic> bics;
	private DualListModel<String> UUMIDCheck;
	private String name;

	public PreConditionView(ConfigurationService service) {
		this.service = service;
		configurations = service.getConfigurations();

		List<Bic> bicsSource = service.getBics();
		List<Bic> bicsDestination = new ArrayList<Bic>();

		List<String> UUMIDCheckSource = service.getUUMIDFileds();
		List<String> UUMIDCheckDestination = new ArrayList<String>();

		bics = new DualListModel<Bic>(bicsSource, bicsDestination);
		setName("ahmed");
		setUUMIDCheck(new DualListModel<String>(UUMIDCheckSource, UUMIDCheckDestination));

	}

	public void onTransferBic(TransferEvent event) {
		StringBuilder builder = new StringBuilder();
		for (Object item : event.getItems()) {
			builder.append(((Bic) item).getCode()).append("<br />");
		}

		FacesMessage msg = new FacesMessage();
		msg.setSeverity(FacesMessage.SEVERITY_INFO);
		msg.setSummary("Items Transferred");
		msg.setDetail(builder.toString());

		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public void onSelectBic(SelectEvent<Bic> event) {
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_INFO, "Item Selected", event.getObject().getCode()));
	}

	public void onUnselectBic(UnselectEvent<Bic> event) {
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_INFO, "Item Unselected", event.getObject().getCode()));
	}

	public void onReorderBic() {
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "List Reordered", null));
	}

	public ConfigurationService getService() {
		return service;
	}

	public void setService(ConfigurationService service) {
		this.service = service;
	}

	public DualListModel<Bic> getBics() {
		return bics;
	}

	public void setBics(DualListModel<Bic> bics) {
		this.bics = bics;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Configuration> getConfigurations() {
		return configurations;
	}

	public void setConfigurations(List<Configuration> configurations) {
		this.configurations = configurations;
	}

	public DualListModel<String> getUUMIDCheck() {
		return UUMIDCheck;
	}

	public void setUUMIDCheck(DualListModel<String> uUMIDCheck) {
		UUMIDCheck = uUMIDCheck;
	}

	/*
	 * public DualListModel<UUMIDFiledsEnum> getUUMIDCheck() { return UUMIDCheck; }
	 * 
	 * public void setUUMIDCheck(DualListModel<UUMIDFiledsEnum> uUMIDCheck) {
	 * UUMIDCheck = uUMIDCheck; }
	 */

}
