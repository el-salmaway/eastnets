package com.en.configManager.views.addedit.settings;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import com.en.common.enums.PurgeKeepRemoveEnum;

@ApplicationScoped
@ManagedBean
public class PurgeKeepRemove {
	
	  public PurgeKeepRemoveEnum[] getStatuses() {
	        return PurgeKeepRemoveEnum.values();
	    }

}
