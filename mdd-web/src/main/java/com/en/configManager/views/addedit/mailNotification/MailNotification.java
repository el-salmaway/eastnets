package com.en.configManager.views.addedit.mailNotification;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.en.configManager.beans.Bic;
import com.en.configManager.service.ConfigurationService;


public class MailNotification implements Serializable {

	/**
	* 
	*/
	private static final long serialVersionUID = 1L;
	private Bic bic;
	private Map<Bic, Integer> bicsMap;
	private List<Bic> bics;
    private boolean enabled;

 	private ConfigurationService service;

	
 	
 	
	public  MailNotification(ConfigurationService service) {
		this.service=service;
		bics = service.getBics();
		bicsMap = bics.stream().collect(Collectors.toMap(bic -> bic, Bic::getId));

	}

	public Bic getBic() {
		return bic;
	}

	public void setBic(Bic bic) {
		this.bic = bic;
	}

	public Map<Bic, Integer> getBicsMap() {
		return bicsMap;
	}

	public void setBicsMap(Map<Bic, Integer> bicsMap) {
		this.bicsMap = bicsMap;
	}

	public List<Bic> getBics() {
		return bics;
	}

	public void setBics(List<Bic> bics) {
		this.bics = bics;
	}

	public ConfigurationService getService() {
		return service;
	}

	public void setService(ConfigurationService service) {
		this.service = service;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}