package com.en.configManager.dao.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;



@Entity
@Table(name = "STXGETFIELDSVIEW")
public class STXGetFieldsView {


     
    private static final long serialVersionUID = -8014079474097828807L;

    @Id
    @Column(unique = true, nullable = false, precision = 10)
    private Long idx;

    @Column(name = "CODE",nullable = false, precision = 3)
    private Long code;

    @Column(name = "CODE_ID", nullable = false, precision = 3)
    private Long codeId;

    @Column(name = "ENTRY_ALTERNATE", length = 32)
    private String entryAlternate;

    @Column(name = "ENTRY_ALTERNATE_CHOICE", length = 2)
    private String entryAlternateChoice;

    @Column(name = "ENTRY_ID", nullable = false, length = 40)
    private String entryId;

    @Column(name = "ENTRY_OPTION", nullable = false, length = 1)
    private String entryOption;

    @Column(name = "EXPANSION",length = 132)
    private String expansion;

    @Column(name = "FIELD_CNT", nullable = false, precision = 4)
    private Long fieldCnt;

    @Column(name = "IS_OPTIONAL", nullable = false, precision = 1)
    private short isOptional;

    @Column(name = "LOOP_ID", nullable = false, precision = 10)
    private Long loopId;



    @Column(name = "SEQUENCE_ID", nullable = false, length = 1)
    private String sequenceId;

    @Column(name="TAG" ,  length = 4)
    private String tag;

    @Column(name="TYPE" ,  length = 4)
    private String type;

    @Column(name = "TYPE_IDX", nullable = false, precision = 10)
    private Long typeIdx;

    @Column(name = "CONFIG_ID", nullable = false, precision = 4)
    private Long configId;


    @Column(name = "MESG_TYPE", nullable = false, length = 3,columnDefinition = "CHAR (3 BYTE)")
    private String mesgType;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getConfigId() {
        return configId;
    }

    public void setConfigId(Long configId) {
        this.configId = configId;
    }

    public String getMesgType() {
        return mesgType;
    }

    public void setMesgType(String mesgType) {
        this.mesgType = mesgType;
    }

    public Long getIdx() {
        return idx;
    }

    public void setIdx(Long idx) {
        this.idx = idx;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public Long getCodeId() {
        return codeId;
    }

    public void setCodeId(Long codeId) {
        this.codeId = codeId;
    }

    public String getEntryAlternate() {
        return entryAlternate;
    }

    public void setEntryAlternate(String entryAlternate) {
        this.entryAlternate = entryAlternate;
    }

    public String getEntryAlternateChoice() {
        return entryAlternateChoice;
    }

    public void setEntryAlternateChoice(String entryAlternateChoice) {
        this.entryAlternateChoice = entryAlternateChoice;
    }

    public String getEntryId() {
        return entryId;
    }

    public void setEntryId(String entryId) {
        this.entryId = entryId;
    }

    public String getEntryOption() {
        return entryOption;
    }

    public void setEntryOption(String entryOption) {
        this.entryOption = entryOption;
    }

    public String getExpansion() {
        return expansion;
    }

    public void setExpansion(String expansion) {
        this.expansion = expansion;
    }

    public Long getFieldCnt() {
        return fieldCnt;
    }

    public void setFieldCnt(Long fieldCnt) {
        this.fieldCnt = fieldCnt;
    }

    public short getIsOptional() {
        return isOptional;
    }

    public void setIsOptional(short isOptional) {
        this.isOptional = isOptional;
    }

    public Long getLoopId() {
        return loopId;
    }

    public void setLoopId(Long loopId) {
        this.loopId = loopId;
    }


    public String getSequenceId() {
        return sequenceId;
    }

    public void setSequenceId(String sequenceId) {
        this.sequenceId = sequenceId;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Long getTypeIdx() {
        return typeIdx;
    }

    public void setTypeIdx(Long typeIdx) {
        this.typeIdx = typeIdx;
    }







    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((code == null) ? 0 : code.hashCode());
        result = prime * result + ((codeId == null) ? 0 : codeId.hashCode());
        result = prime * result + ((entryAlternate == null) ? 0 : entryAlternate.hashCode());
        result = prime * result + ((entryAlternateChoice == null) ? 0 : entryAlternateChoice.hashCode());
        result = prime * result + ((entryId == null) ? 0 : entryId.hashCode());
        result = prime * result + ((entryOption == null) ? 0 : entryOption.hashCode());
        result = prime * result + ((expansion == null) ? 0 : expansion.hashCode());
        result = prime * result + ((fieldCnt == null) ? 0 : fieldCnt.hashCode());
        result = prime * result + isOptional;
        result = prime * result + ((loopId == null) ? 0 : loopId.hashCode());
        result = prime * result + ((sequenceId == null) ? 0 : sequenceId.hashCode());
        result = prime * result + ((tag == null) ? 0 : tag.hashCode());
        result = prime * result + ((typeIdx == null) ? 0 : typeIdx.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj){return false;} /*{
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SyntaxEntryField other = (SyntaxEntryField) obj;
        if (code == null) {
            if (other.code != null)
                return false;
        } else if (!code.equals(other.code))
            return false;
        if (codeId == null) {
            if (other.codeId != null)
                return false;
        } else if (!codeId.equals(other.codeId))
            return false;
        if (entryAlternate == null) {
            if (other.entryAlternate != null)
                return false;
        } else if (!entryAlternate.equals(other.entryAlternate))
            return false;
        if (entryAlternateChoice == null) {
            if (other.entryAlternateChoice != null)
                return false;
        } else if (!entryAlternateChoice.equals(other.entryAlternateChoice))
            return false;
        if (entryId == null) {
            if (other.entryId != null)
                return false;
        } else if (!entryId.equals(other.entryId))
            return false;
        if (entryOption == null) {
            if (other.entryOption != null)
                return false;
        } else if (!entryOption.equals(other.entryOption))
            return false;
        if (expansion == null) {
            if (other.expansion != null)
                return false;
        } else if (!expansion.equals(other.expansion))
            return false;
        if (fieldCnt == null) {
            if (other.fieldCnt != null)
                return false;
        } else if (!fieldCnt.equals(other.fieldCnt))
            return false;
        if (isOptional != other.isOptional)
            return false;
        if (loopId == null) {
            if (other.loopId != null)
                return false;
        } else if (!loopId.equals(other.loopId))
            return false;
        if (pattId == null) {
            if (other.pattId != null)
                return false;
        } else if (!pattId.equals(other.pattId))
            return false;
        if (sequenceId == null) {
            if (other.sequenceId != null)
                return false;
        } else if (!sequenceId.equals(other.sequenceId))
            return false;
        if (stxPats == null) {
            if (other.stxPats != null)
                return false;
        } else if (!stxPats.equals(other.stxPats))
            return false;
        if (tag == null) {
            if (other.tag != null)
                return false;
        } else if (!tag.equals(other.tag))
            return false;
        if (typeIdx == null) {
            if (other.typeIdx != null)
                return false;
        } else if (!typeIdx.equals(other.typeIdx))
            return false;
        return true;
    }*/

    @Override
    public String toString() {
        return "SyntaxEntryField [idx=" + idx + ", code=" + code + ", codeId=" + codeId + ", entryAlternate="
                + entryAlternate + ", entryAlternateChoice=" + entryAlternateChoice + ", entryId=" + entryId
                + ", entryOption=" + entryOption + ", expansion=" + expansion + ", fieldCnt=" + fieldCnt
                + ", isOptional=" + isOptional + ", loopId=" + loopId  + ", sequenceId="
                + sequenceId + ", tag=" + tag + ", typeIdx=" + typeIdx ;
    }







}















