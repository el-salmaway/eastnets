package com.en.configManager.dao.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "STXENTRYLOOP")
public class SyntaxEntryLoop implements Comparable<SyntaxEntryLoop>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7035676903916391527L;

	@EmbeddedId
	private SyntaxEntryLoopPK id;

	@Column(name = "ENTRY_ID", nullable = false, length = 40)
	private String entryId;

	@Column(name = "MAX_OCC", nullable = false, precision = 5)
	private Long maxOcc;

	@Column(name = "MIN_OCC", nullable = false, precision = 5)
	private Integer minOcc;

	@ManyToOne
	@JoinColumn(name = "TYPE_IDX", nullable = false, insertable = false, updatable = false)
	private SyntaxMessage stxMessage;

	public SyntaxEntryLoopPK getId() {
		return id;
	}

	public void setId(SyntaxEntryLoopPK id) {
		this.id = id;
	}

	public String getEntryId() {
		return entryId;
	}

	public void setEntryId(String entryId) {
		this.entryId = entryId;
	}

	public Long getMaxOcc() {
		return maxOcc;
	}

	public void setMaxOcc(Long maxOcc) {
		this.maxOcc = maxOcc;
	}

	public Integer getMinOcc() {
		return minOcc;
	}

	public void setMinOcc(Integer minOcc) {
		this.minOcc = minOcc;
	}

	public SyntaxMessage getStxMessage() {
		return stxMessage;
	}

	public void setStxMessage(SyntaxMessage stxMessage) {
		this.stxMessage = stxMessage;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((entryId == null) ? 0 : entryId.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((maxOcc == null) ? 0 : maxOcc.hashCode());
		result = prime * result + ((minOcc == null) ? 0 : minOcc.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SyntaxEntryLoop other = (SyntaxEntryLoop) obj;
		if (entryId == null) {
			if (other.entryId != null)
				return false;
		} else if (!entryId.equals(other.entryId))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (maxOcc == null) {
			if (other.maxOcc != null)
				return false;
		} else if (!maxOcc.equals(other.maxOcc))
			return false;
		if (minOcc == null) {
			if (other.minOcc != null)
				return false;
		} else if (!minOcc.equals(other.minOcc))
			return false;
		return true;
	}

	@Override
	public int compareTo(SyntaxEntryLoop o) {
		return entryId.compareTo(o.entryId);
	}

}
