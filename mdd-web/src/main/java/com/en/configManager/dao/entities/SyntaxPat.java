package com.en.configManager.dao.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "STXPATS")
public class SyntaxPat implements Comparable<SyntaxPat>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1694891510040177470L;

	@EmbeddedId
	private SyntaxPatPK id = new SyntaxPatPK();

	@Column(name = "DEFAULT_VALUE", length = 12)
	private String defaultValue;

	@Column(name = "EXISTENCE_CONDITION", length = 24)
	private String existenceCondition;

	@Column(name = "IS_EDITABLE", nullable = false, precision = 1)
	private short isEditable;

	@Column(name = "IS_OPTIONAL", nullable = false, precision = 1)
	private short isOptional;

	@Column(name = "IS_VERIFIABLE", nullable = false, precision = 1)
	private short isVerifiable;

	@Column(name = "IS_VISIBLE", nullable = false, precision = 1)
	private short isVisible;

	@Column(name = "MAX_CHAR", nullable = false, precision = 5)
	private short maxChar;

	@Column(name = "MIN_CHAR", nullable = false, precision = 5)
	private short minChar;

	@Column(name = "NB_ROWS", nullable = false, precision = 5)
	private short nbRows;

	@Column(name = "PATT_ID", nullable = false, length = 32)
	private String pattId;

	@Column(name = "PROMPT", length = 64)
	private String prompt;

	@Column(name = "ROW_SEPARATOR", length = 12)
	private String rowSeparator;

	@Column(name = "TYPE", nullable = false, length = 32)
	private String type;

	@ManyToOne
	@JoinColumn(name = "FIELD_IDX", nullable = false, insertable = false, updatable = false)
	private SyntaxEntryField stxEntryField;

	public SyntaxPatPK getId() {
		return id;
	}

	public void setId(SyntaxPatPK id) {
		this.id = id;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public String getExistenceCondition() {
		return existenceCondition;
	}

	public void setExistenceCondition(String existenceCondition) {
		this.existenceCondition = existenceCondition;
	}

	public short getIsEditable() {
		return isEditable;
	}

	public void setIsEditable(short isEditable) {
		this.isEditable = isEditable;
	}

	public short getIsOptional() {
		return isOptional;
	}

	public void setIsOptional(short isOptional) {
		this.isOptional = isOptional;
	}

	public short getIsVerifiable() {
		return isVerifiable;
	}

	public void setIsVerifiable(short isVerifiable) {
		this.isVerifiable = isVerifiable;
	}

	public short getIsVisible() {
		return isVisible;
	}

	public void setIsVisible(short isVisible) {
		this.isVisible = isVisible;
	}

	public short getMaxChar() {
		return maxChar;
	}

	public void setMaxChar(short maxChar) {
		this.maxChar = maxChar;
	}

	public short getMinChar() {
		return minChar;
	}

	public void setMinChar(short minChar) {
		this.minChar = minChar;
	}

	public short getNbRows() {
		return nbRows;
	}

	public void setNbRows(short nbRows) {
		this.nbRows = nbRows;
	}

	public String getPattId() {
		return pattId;
	}

	public void setPattId(String pattId) {
		this.pattId = pattId;
	}

	public String getPrompt() {
		return prompt;
	}

	public void setPrompt(String prompt) {
		this.prompt = prompt;
	}

	public String getRowSeparator() {
		return rowSeparator;
	}

	public void setRowSeparator(String rowSeparator) {
		this.rowSeparator = rowSeparator;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public SyntaxEntryField getStxEntryField() {
		return stxEntryField;
	}

	public void setStxEntryField(SyntaxEntryField stxEntryField) {
		this.stxEntryField = stxEntryField;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((defaultValue == null) ? 0 : defaultValue.hashCode());
		result = prime * result + ((existenceCondition == null) ? 0 : existenceCondition.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + isEditable;
		result = prime * result + isOptional;
		result = prime * result + isVerifiable;
		result = prime * result + isVisible;
		result = prime * result + maxChar;
		result = prime * result + minChar;
		result = prime * result + nbRows;
		result = prime * result + ((pattId == null) ? 0 : pattId.hashCode());
		result = prime * result + ((prompt == null) ? 0 : prompt.hashCode());
		result = prime * result + ((rowSeparator == null) ? 0 : rowSeparator.hashCode());
		result = prime * result + ((stxEntryField == null) ? 0 : stxEntryField.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SyntaxPat other = (SyntaxPat) obj;
		if (defaultValue == null) {
			if (other.defaultValue != null)
				return false;
		} else if (!defaultValue.equals(other.defaultValue))
			return false;
		if (existenceCondition == null) {
			if (other.existenceCondition != null)
				return false;
		} else if (!existenceCondition.equals(other.existenceCondition))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (isEditable != other.isEditable)
			return false;
		if (isOptional != other.isOptional)
			return false;
		if (isVerifiable != other.isVerifiable)
			return false;
		if (isVisible != other.isVisible)
			return false;
		if (maxChar != other.maxChar)
			return false;
		if (minChar != other.minChar)
			return false;
		if (nbRows != other.nbRows)
			return false;
		if (pattId == null) {
			if (other.pattId != null)
				return false;
		} else if (!pattId.equals(other.pattId))
			return false;
		if (prompt == null) {
			if (other.prompt != null)
				return false;
		} else if (!prompt.equals(other.prompt))
			return false;
		if (rowSeparator == null) {
			if (other.rowSeparator != null)
				return false;
		} else if (!rowSeparator.equals(other.rowSeparator))
			return false;
		if (stxEntryField == null) {
			if (other.stxEntryField != null)
				return false;
		} else if (!stxEntryField.equals(other.stxEntryField))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

	@Override
	public int compareTo(SyntaxPat o) {
		int compare = pattId.compareTo(o.pattId);
		if (compare == 0)
			compare = id.getFieldIdx().compareTo(o.id.getFieldIdx());
		return compare;
	}

}
