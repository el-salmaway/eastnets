package com.en.configManager.dao.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class SyntaxAlternateChoicePK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1797176761907247132L;

	@Column(name = "TYPE_IDX", unique = false, nullable = false, precision = 10)
	private long typeIdx;

	@Column(name = "LOOP_ID", unique = false, nullable = false, precision = 10)
	private long loopId;

	@Column(name = "SEQUENCE_ID", unique = false, nullable = false, length = 1)
	private String sequenceId;

	@Column(name = "ENTRY_ID", unique = false, nullable = false, length = 32)
	private String entryId;

	@Column(name = "OPTION_CHOICE", unique = true, nullable = false, length = 2)
	private String optionChoice;

	public long getTypeIdx() {
		return typeIdx;
	}

	public void setTypeIdx(long typeIdx) {
		this.typeIdx = typeIdx;
	}

	public long getLoopId() {
		return loopId;
	}

	public void setLoopId(long loopId) {
		this.loopId = loopId;
	}

	public String getSequenceId() {
		return sequenceId;
	}

	public void setSequenceId(String sequenceId) {
		this.sequenceId = sequenceId;
	}

	public String getEntryId() {
		return entryId;
	}

	public void setEntryId(String entryId) {
		this.entryId = entryId;
	}

	public String getOptionChoice() {
		return optionChoice;
	}

	public void setOptionChoice(String optionChoice) {
		this.optionChoice = optionChoice;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((entryId == null) ? 0 : entryId.hashCode());
		result = prime * result + (int) (loopId ^ (loopId >>> 32));
		result = prime * result + ((optionChoice == null) ? 0 : optionChoice.hashCode());
		result = prime * result + ((sequenceId == null) ? 0 : sequenceId.hashCode());
		result = prime * result + (int) (typeIdx ^ (typeIdx >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SyntaxAlternateChoicePK other = (SyntaxAlternateChoicePK) obj;
		if (entryId == null) {
			if (other.entryId != null)
				return false;
		} else if (!entryId.equals(other.entryId))
			return false;
		if (loopId != other.loopId)
			return false;
		if (optionChoice == null) {
			if (other.optionChoice != null)
				return false;
		} else if (!optionChoice.equals(other.optionChoice))
			return false;
		if (sequenceId == null) {
			if (other.sequenceId != null)
				return false;
		} else if (!sequenceId.equals(other.sequenceId))
			return false;
		if (typeIdx != other.typeIdx)
			return false;
		return true;
	}

}
