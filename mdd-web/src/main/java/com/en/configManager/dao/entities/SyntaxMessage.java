package com.en.configManager.dao.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;
import java.util.TreeSet;

/**
 * 
 * @author ahammad
 *
 */

@Entity
@Table(name = "STXMESSAGE")
public class SyntaxMessage implements Comparable<SyntaxMessage>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3337616220346310570L;

	private static long idNextValue;
	public SyntaxMessage(){
		this.idx=++SyntaxMessage.idNextValue;


	}

	@Id
	@Column(unique = true, nullable = false, precision = 10)
	private Long idx;

	@Column(nullable = false, length = 132)
	private String description;

	@Column(name = "TYPE", nullable = false, length = 3)
	private String type;

	@OneToMany(mappedBy = "stxMessage", cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE })
	private Set<SyntaxAlternateChoice> stxAlternateChoices = new TreeSet<SyntaxAlternateChoice>();

	@OneToMany(mappedBy = "stxMessage", cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE })
	private Set<SyntaxEntryAlternate> stxEntryAlternates = new TreeSet<SyntaxEntryAlternate>();

	@OneToMany(mappedBy = "stxMessage", cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE })
	private Set<SyntaxEntryLoop> stxEntryLoops = new TreeSet<SyntaxEntryLoop>();

	@OneToMany(mappedBy = "stxMessage", cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE })
	private Set<SyntaxEntryOption> stxEntryOptions = new TreeSet<SyntaxEntryOption>();

	@OneToMany(mappedBy = "stxMessage", cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE })
	private Set<SyntaxEntrySequence> stxEntrySequences = new TreeSet<SyntaxEntrySequence>();

	@Transient
	private Set<SyntaxEntryField> messageFields = new TreeSet<SyntaxEntryField>();

	@ManyToOne
	@JoinColumn(name = "VERSION_IDX", nullable = false)
	private SyntaxVersion stxVersion;

	@OneToMany(mappedBy = "stxMessage", cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE })
	private Set<SyntaxOptionChoice> stxOptionChoices = new TreeSet<SyntaxOptionChoice>();

	public Long getIdx() {
		return idx;
	}

	public void setIdx(Long idx) {
		this.idx = idx;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Set<SyntaxAlternateChoice> getStxAlternateChoices() {
		return stxAlternateChoices;
	}

	public void setStxAlternateChoices(Set<SyntaxAlternateChoice> stxAlternateChoices) {
		this.stxAlternateChoices = stxAlternateChoices;
	}

	public Set<SyntaxEntryAlternate> getStxEntryAlternates() {
		return stxEntryAlternates;
	}

	public void setStxEntryAlternates(Set<SyntaxEntryAlternate> stxEntryAlternates) {
		this.stxEntryAlternates = stxEntryAlternates;
	}

	public Set<SyntaxEntryLoop> getStxEntryLoops() {
		return stxEntryLoops;
	}

	public void setStxEntryLoops(Set<SyntaxEntryLoop> stxEntryLoops) {
		this.stxEntryLoops = stxEntryLoops;
	}

	public Set<SyntaxEntryOption> getStxEntryOptions() {
		return stxEntryOptions;
	}

	public void setStxEntryOptions(Set<SyntaxEntryOption> stxEntryOptions) {
		this.stxEntryOptions = stxEntryOptions;
	}

	public Set<SyntaxEntrySequence> getStxEntrySequences() {
		return stxEntrySequences;
	}

	public void setStxEntrySequences(Set<SyntaxEntrySequence> stxEntrySequences) {
		this.stxEntrySequences = stxEntrySequences;
	}

	public Set<SyntaxEntryField> getMessageFields() {
		return messageFields;
	}

	public void setMessageFields(Set<SyntaxEntryField> messageFields) {
		this.messageFields = messageFields;
	}

	public SyntaxVersion getStxVersion() {
		return stxVersion;
	}

	public void setStxVersion(SyntaxVersion stxVersion) {
		this.stxVersion = stxVersion;
	}

	public Set<SyntaxOptionChoice> getStxOptionChoices() {
		return stxOptionChoices;
	}

	public void setStxOptionChoices(Set<SyntaxOptionChoice> stxOptionChoices) {
		this.stxOptionChoices = stxOptionChoices;
	}

	@Override
	public String toString() {
		return "SyntaxMessage [description=" + description + ", type=" + type + ", messageFields=" + messageFields
				+ ", stxVersion=" + stxVersion + "]";
	}

	@Override
	public int compareTo(SyntaxMessage o) {
		// TODO Auto-generated method stub
		return type.compareTo(o.getType());
	}

}
