package com.en.configManager.dao.repos;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.en.configManager.dao.entities.SyntaxVersion;

@Repository
public interface SyntaxRepository extends CrudRepository<SyntaxVersion, Long> {

	@Query("SELECT coalesce(max(sv.idx), 0) FROM SyntaxVersion sv")
	Long getMaxId();

}
