package com.en.configManager.dao.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;
import java.util.TreeSet;

@Entity
@Table(name = "STXENTRYOPTION")
public class SyntaxEntryOption implements Comparable<SyntaxEntryOption>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -489042523564505625L;

	@EmbeddedId
	private SyntaxEntryOptionPK id;

	@Column(name = "FIELD_TAG", nullable = false, length = 4)
	private String fieldTag;

	@Column(name = "IS_OPTIONAL", nullable = false, precision = 1)
	private short isOptional;

	@Column(name = "PATT_ID", length = 24)
	private String pattId;

	@ManyToOne
	@JoinColumn(name = "TYPE_IDX", nullable = false, insertable = false, updatable = false)
	private SyntaxMessage stxMessage;

	@OneToMany(mappedBy = "stxEntryOption")
	private Set<SyntaxOptionChoice> stxOptionChoices = new TreeSet<SyntaxOptionChoice>();

	public SyntaxEntryOptionPK getId() {
		return id;
	}

	public void setId(SyntaxEntryOptionPK id) {
		this.id = id;
	}

	public String getFieldTag() {
		return fieldTag;
	}

	public void setFieldTag(String fieldTag) {
		this.fieldTag = fieldTag;
	}

	public short getIsOptional() {
		return isOptional;
	}

	public void setIsOptional(short isOptional) {
		this.isOptional = isOptional;
	}

	public String getPattId() {
		return pattId;
	}

	public void setPattId(String pattId) {
		this.pattId = pattId;
	}

	public SyntaxMessage getStxMessage() {
		return stxMessage;
	}

	public void setStxMessage(SyntaxMessage stxMessage) {
		this.stxMessage = stxMessage;
	}

	public Set<SyntaxOptionChoice> getStxOptionChoices() {
		return stxOptionChoices;
	}

	public void setStxOptionChoices(Set<SyntaxOptionChoice> stxOptionChoices) {
		this.stxOptionChoices = stxOptionChoices;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fieldTag == null) ? 0 : fieldTag.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + isOptional;
		result = prime * result + ((pattId == null) ? 0 : pattId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SyntaxEntryOption other = (SyntaxEntryOption) obj;
		if (fieldTag == null) {
			if (other.fieldTag != null)
				return false;
		} else if (!fieldTag.equals(other.fieldTag))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (isOptional != other.isOptional)
			return false;
		if (pattId == null) {
			if (other.pattId != null)
				return false;
		} else if (!pattId.equals(other.pattId))
			return false;
		return true;
	}

	@Override
	public int compareTo(SyntaxEntryOption o) {
		// TODO Auto-generated method stub
		int compareTo = id.getCode().compareTo(o.id.getCode());
		if (compareTo == 0)
			compareTo = id.getLoopId().compareTo(o.id.getLoopId());
		if (compareTo == 0)
			compareTo = id.getSequenceId().compareTo(o.id.getSequenceId());
		return compareTo;

	}

}
