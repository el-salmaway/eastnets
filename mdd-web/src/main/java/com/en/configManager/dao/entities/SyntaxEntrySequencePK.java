package com.en.configManager.dao.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class SyntaxEntrySequencePK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5550818118600211233L;

	@Column(name = "TYPE_IDX", unique = false, nullable = false, precision = 10)
	private long typeIdx;

	@Column(unique = false, nullable = false, length = 1)
	private String id;

	public long getTypeIdx() {
		return typeIdx;
	}

	public void setTypeIdx(long typeIdx) {
		this.typeIdx = typeIdx;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + (int) (typeIdx ^ (typeIdx >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SyntaxEntrySequencePK other = (SyntaxEntrySequencePK) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (typeIdx != other.typeIdx)
			return false;
		return true;
	}

}
