package com.en.configManager.dao.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class SyntaxEntryLoopPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7614599305465501056L;

	@Column(name = "TYPE_IDX", unique = false, nullable = false, precision = 10)
	private long typeIdx;

	@Column(unique = false, nullable = false, precision = 10)
	private long id;
	@Column(name = "SEQUENCE_ID", unique = false, nullable = false, length = 1)
	private String sequenceId;

	public long getTypeIdx() {
		return typeIdx;
	}

	public void setTypeIdx(long typeIdx) {
		this.typeIdx = typeIdx;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSequenceId() {
		return sequenceId;
	}

	public void setSequenceId(String sequenceId) {
		this.sequenceId = sequenceId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((sequenceId == null) ? 0 : sequenceId.hashCode());
		result = prime * result + (int) (typeIdx ^ (typeIdx >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SyntaxEntryLoopPK other = (SyntaxEntryLoopPK) obj;
		if (id != other.id)
			return false;
		if (sequenceId == null) {
			if (other.sequenceId != null)
				return false;
		} else if (!sequenceId.equals(other.sequenceId))
			return false;
		if (typeIdx != other.typeIdx)
			return false;
		return true;
	}

}
