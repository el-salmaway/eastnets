package com.en.configManager.dao.repos;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.en.configManager.dao.entities.SyntaxEntryOption;
import com.en.configManager.dao.entities.SyntaxEntryOptionPK;

@Repository
public interface SyntaxEntryOptionRepository extends CrudRepository<SyntaxEntryOption, SyntaxEntryOptionPK> {

}
