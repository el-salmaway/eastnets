package com.en.configManager.dao.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "STXALTERNATECHOICE")
public class SyntaxAlternateChoice implements Comparable<SyntaxAlternateChoice>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7346023920773404079L;

	@EmbeddedId
	private SyntaxAlternateChoicePK id;

	@ManyToOne
	@JoinColumns({ @JoinColumn(name = "ENTRY_ID", referencedColumnName = "ENTRY_ID", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "LOOP_ID", referencedColumnName = "LOOP_ID", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "SEQUENCE_ID", referencedColumnName = "SEQUENCE_ID", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "TYPE_IDX", referencedColumnName = "TYPE_IDX", nullable = false, insertable = false, updatable = false) })
	private SyntaxEntryAlternate stxEntryAlternate;

	@ManyToOne
	@JoinColumn(name = "TYPE_IDX", nullable = false, insertable = false, updatable = false)
	private SyntaxMessage stxMessage;

	public SyntaxAlternateChoicePK getId() {
		return id;
	}

	public void setId(SyntaxAlternateChoicePK id) {
		this.id = id;
	}

	public SyntaxEntryAlternate getStxEntryAlternate() {
		return stxEntryAlternate;
	}

	public void setStxEntryAlternate(SyntaxEntryAlternate stxEntryAlternate) {
		this.stxEntryAlternate = stxEntryAlternate;
	}

	public SyntaxMessage getStxMessage() {
		return stxMessage;
	}

	public void setStxMessage(SyntaxMessage stxMessage) {
		this.stxMessage = stxMessage;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SyntaxAlternateChoice other = (SyntaxAlternateChoice) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public int compareTo(SyntaxAlternateChoice o) {
		return id.getOptionChoice().compareTo(o.id.getOptionChoice());
	}

}
