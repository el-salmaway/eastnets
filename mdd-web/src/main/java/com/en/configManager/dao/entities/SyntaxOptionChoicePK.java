package com.en.configManager.dao.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class SyntaxOptionChoicePK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2516298777202447374L;

	@Column(name = "TYPE_IDX", unique = false, nullable = false, precision = 10)
	private long typeIdx;

	@Column(unique = false, nullable = false, precision = 3)
	private Long code;

	@Column(name = "CODE_ID", unique = false, nullable = false, precision = 3)
	private long codeId;

	@Column(name = "LOOP_ID", unique = false, nullable = false, precision = 10)
	private long loopId;

	@Column(name = "SEQUENCE_ID", unique = false, nullable = false, length = 1)
	private String sequenceId;

	@Column(name = "OPTION_CHOICE", unique = false, nullable = false, length = 1)
	private String optionChoice;

	public long getTypeIdx() {
		return typeIdx;
	}

	public void setTypeIdx(long typeIdx) {
		this.typeIdx = typeIdx;
	}

	public Long getCode() {
		return code;
	}

	public void setCode(Long code) {
		this.code = code;
	}

	public long getCodeId() {
		return codeId;
	}

	public void setCodeId(long codeId) {
		this.codeId = codeId;
	}

	public Long getLoopId() {
		return loopId;
	}

	public void setLoopId(long loopId) {
		this.loopId = loopId;
	}

	public String getSequenceId() {
		return sequenceId;
	}

	public void setSequenceId(String sequenceId) {
		this.sequenceId = sequenceId;
	}

	public String getOptionChoice() {
		return optionChoice;
	}

	public void setOptionChoice(String optionChoice) {
		this.optionChoice = optionChoice;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (code ^ (code >>> 32));
		result = prime * result + (int) (codeId ^ (codeId >>> 32));
		result = prime * result + (int) (loopId ^ (loopId >>> 32));
		result = prime * result + ((optionChoice == null) ? 0 : optionChoice.hashCode());
		result = prime * result + ((sequenceId == null) ? 0 : sequenceId.hashCode());
		result = prime * result + (int) (typeIdx ^ (typeIdx >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SyntaxOptionChoicePK other = (SyntaxOptionChoicePK) obj;
		if (code != other.code)
			return false;
		if (codeId != other.codeId)
			return false;
		if (loopId != other.loopId)
			return false;
		if (optionChoice == null) {
			if (other.optionChoice != null)
				return false;
		} else if (!optionChoice.equals(other.optionChoice))
			return false;
		if (sequenceId == null) {
			if (other.sequenceId != null)
				return false;
		} else if (!sequenceId.equals(other.sequenceId))
			return false;
		if (typeIdx != other.typeIdx)
			return false;
		return true;
	}

}
