package com.en.configManager.dao.repos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.en.configManager.dao.entities.STXGetFieldsView;

public interface STXGetFiedsViewRepo extends JpaRepository<STXGetFieldsView,Long> {

    @Query("select e from STXGetFieldsView e where e.type= :type")
    public List<STXGetFieldsView> getfields(@Param(value = "type") String type);







}
