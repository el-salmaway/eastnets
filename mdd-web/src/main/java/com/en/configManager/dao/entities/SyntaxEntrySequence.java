package com.en.configManager.dao.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "STXENTRYSEQUENCE")
public class SyntaxEntrySequence implements Comparable<SyntaxEntrySequence>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8846693075203557153L;

	@EmbeddedId
	private SyntaxEntrySequencePK id;

	@Column(name = "ENTRY_ID", nullable = false, length = 2)
	private String entryId;

	@Column(length = 132)
	private String expansion;
	@Column(name = "IS_OPTIONAL", nullable = false, precision = 1)
	private short isOptional;

	@ManyToOne
	@JoinColumn(name = "TYPE_IDX", nullable = false, insertable = false, updatable = false)
	private SyntaxMessage stxMessage;

	public SyntaxEntrySequencePK getId() {
		return id;
	}

	public void setId(SyntaxEntrySequencePK id) {
		this.id = id;
	}

	public String getEntryId() {
		return entryId;
	}

	public void setEntryId(String entryId) {
		this.entryId = entryId;
	}

	public String getExpansion() {
		return expansion;
	}

	public void setExpansion(String expansion) {
		this.expansion = expansion;
	}

	public short getIsOptional() {
		return isOptional;
	}

	public void setIsOptional(short isOptional) {
		this.isOptional = isOptional;
	}

	public SyntaxMessage getStxMessage() {
		return stxMessage;
	}

	public void setStxMessage(SyntaxMessage stxMessage) {
		this.stxMessage = stxMessage;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((entryId == null) ? 0 : entryId.hashCode());
		result = prime * result + ((expansion == null) ? 0 : expansion.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + isOptional;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SyntaxEntrySequence other = (SyntaxEntrySequence) obj;
		if (entryId == null) {
			if (other.entryId != null)
				return false;
		} else if (!entryId.equals(other.entryId))
			return false;
		if (expansion == null) {
			if (other.expansion != null)
				return false;
		} else if (!expansion.equals(other.expansion))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (isOptional != other.isOptional)
			return false;
		return true;
	}

	@Override
	public int compareTo(SyntaxEntrySequence o) {
		return this.id.getId().compareTo(o.id.getId());
	}

}
