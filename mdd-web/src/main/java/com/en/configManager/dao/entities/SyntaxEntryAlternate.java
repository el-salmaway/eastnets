package com.en.configManager.dao.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "STXENTRYALTERNATE")
public class SyntaxEntryAlternate implements Comparable<SyntaxEntryAlternate>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3493486994544397124L;

	@EmbeddedId
	private SyntaxEntryAlternatePK id;

	@Column(name = "IS_OPTIONAL", nullable = false, precision = 1)
	private short isOptional;

	@OneToMany(mappedBy = "stxEntryAlternate", cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE })
	private List<SyntaxAlternateChoice> stxAlternateChoices = new ArrayList<SyntaxAlternateChoice>();

	@ManyToOne
	@JoinColumn(name = "TYPE_IDX", nullable = false, insertable = false, updatable = false)
	private SyntaxMessage stxMessage;

	public SyntaxEntryAlternatePK getId() {
		return id;
	}

	public void setId(SyntaxEntryAlternatePK id) {
		this.id = id;
	}

	public short getIsOptional() {
		return isOptional;
	}

	public void setIsOptional(short isOptional) {
		this.isOptional = isOptional;
	}

	public List<SyntaxAlternateChoice> getStxAlternateChoices() {
		return stxAlternateChoices;
	}

	public void setStxAlternateChoices(List<SyntaxAlternateChoice> stxAlternateChoices) {
		this.stxAlternateChoices = stxAlternateChoices;
	}

	public SyntaxMessage getStxMessage() {
		return stxMessage;
	}

	public void setStxMessage(SyntaxMessage stxMessage) {
		this.stxMessage = stxMessage;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + isOptional;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SyntaxEntryAlternate other = (SyntaxEntryAlternate) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (isOptional != other.isOptional)
			return false;
		return true;
	}

	@Override
	public int compareTo(SyntaxEntryAlternate o) {
		return id.getEntryId().compareTo(o.id.getEntryId());
	}

}
