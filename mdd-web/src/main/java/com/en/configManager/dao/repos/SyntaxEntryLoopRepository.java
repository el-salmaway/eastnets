package com.en.configManager.dao.repos;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.en.configManager.dao.entities.SyntaxEntryLoop;
import com.en.configManager.dao.entities.SyntaxEntryLoopPK;

@Repository
public interface SyntaxEntryLoopRepository extends CrudRepository<SyntaxEntryLoop, SyntaxEntryLoopPK> {

}
