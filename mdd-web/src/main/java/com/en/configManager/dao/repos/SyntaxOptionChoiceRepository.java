package com.en.configManager.dao.repos;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.en.configManager.dao.entities.SyntaxOptionChoice;
import com.en.configManager.dao.entities.SyntaxOptionChoicePK;

@Repository
public interface SyntaxOptionChoiceRepository extends CrudRepository<SyntaxOptionChoice, SyntaxOptionChoicePK> {

}
