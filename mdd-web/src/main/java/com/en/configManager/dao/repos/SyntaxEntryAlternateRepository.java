package com.en.configManager.dao.repos;

  import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.en.configManager.dao.entities.SyntaxEntryAlternate;
import com.en.configManager.dao.entities.SyntaxEntryAlternatePK;

@Repository
public interface SyntaxEntryAlternateRepository extends CrudRepository<SyntaxEntryAlternate, SyntaxEntryAlternatePK> {

}
