package com.en.configManager.dao.repos;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.en.configManager.dao.entities.SyntaxMessage;

@Repository
public interface SyntaxMessageRepository extends CrudRepository<SyntaxMessage, Long> {

	@Query("SELECT coalesce(max(sm.idx), 0) FROM SyntaxMessage sm")
	Long getMaxId();
}
