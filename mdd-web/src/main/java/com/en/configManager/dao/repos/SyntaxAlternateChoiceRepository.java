package com.en.configManager.dao.repos;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.en.configManager.dao.entities.SyntaxAlternateChoice;
import com.en.configManager.dao.entities.SyntaxAlternateChoicePK;

@Repository
public interface SyntaxAlternateChoiceRepository extends CrudRepository<SyntaxAlternateChoice, SyntaxAlternateChoicePK> {

}
