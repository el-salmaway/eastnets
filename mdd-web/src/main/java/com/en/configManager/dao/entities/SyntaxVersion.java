package com.en.configManager.dao.entities;

import javax.persistence.*;
import java.util.Set;

/**
 * 
 * @author ahammad
 *
 */

@Entity
@Table(name = "STXVERSION")
public class SyntaxVersion {

	@Id
	@Column(name = "IDX", unique = true, nullable = false, precision = 3)
	private Long idx;

	@Column(name = "VERSION", nullable = false, length = 4)
	private String version;

	@OneToMany(mappedBy = "stxVersion", cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE }, fetch = FetchType.LAZY)
	private Set<SyntaxMessage> stxmessages;

	public Long getIdx() {
		return idx;
	}

	public void setIdx(Long idx) {
		this.idx = idx;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Set<SyntaxMessage> getStxmessages() {
		return stxmessages;
	}

	public void setStxmessages(Set<SyntaxMessage> stxmessages) {
		this.stxmessages = stxmessages;
	}
@Override
public String toString() {
	// TODO Auto-generated method stub
	return this.version;
}
}
