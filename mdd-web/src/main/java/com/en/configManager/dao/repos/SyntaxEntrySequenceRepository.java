package com.en.configManager.dao.repos;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.en.configManager.dao.entities.SyntaxEntrySequence;
import com.en.configManager.dao.entities.SyntaxEntrySequencePK;

@Repository
public interface SyntaxEntrySequenceRepository extends CrudRepository<SyntaxEntrySequence, SyntaxEntrySequencePK> {

}
