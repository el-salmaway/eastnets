package com.en.configManager.dao.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class SyntaxPatPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1170841329127621203L;

	@Column(name = "FIELD_IDX", insertable = false, updatable = false, unique = true, nullable = false, precision = 10)
	private Long fieldIdx;

	@Column(unique = true, nullable = false, precision = 3)
	private Long id;

	public Long getFieldIdx() {
		return fieldIdx;
	}

	public void setFieldIdx(Long fieldIdx) {
		this.fieldIdx = fieldIdx;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + (int) (fieldIdx ^ (fieldIdx >>> 32));
		return result;
	}
	  @Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		  SyntaxPatPK o=(SyntaxPatPK)obj;
		return this.getFieldIdx().equals(this.fieldIdx.equals(o.getFieldIdx())&&this.getId().equals(o.getId()));
	}
}
