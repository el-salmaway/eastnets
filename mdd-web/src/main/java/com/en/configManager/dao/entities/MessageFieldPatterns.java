package com.en.configManager.dao.entities;

public class MessageFieldPatterns {

	private String name;
	private String reqexPattern;
	private String fieldDescription;
	private String finFormat;
	private String metaType;
	private String logicalType;
	private int minLength;
	private int maxLength;
	private boolean isInChoiceData;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getReqexPattern() {
		return reqexPattern;
	}

	public void setReqexPattern(String reqexPattern) {
		this.reqexPattern = reqexPattern;
	}

	public String getFieldDescription() {
		return fieldDescription;
	}

	public void setFieldDescription(String fieldDescription) {
		this.fieldDescription = fieldDescription;
	}

	public String getFinFormat() {
		return finFormat;
	}

	public void setFinFormat(String finFormat) {
		this.finFormat = finFormat;
	}

	public int getMinLength() {
		return minLength;
	}

	public void setMinLength(int minLength) {
		this.minLength = minLength;
	}

	public int getMaxLength() {
		return maxLength;
	}

	public void setMaxLength(int maxLength) {
		this.maxLength = maxLength;
	}

	public String getMetaType() {
		return metaType;
	}

	public void setMetaType(String metaType) {
		this.metaType = metaType;
	}

	public String getLogicalType() {
		return logicalType;
	}

	public void setLogicalType(String logicalType) {
		this.logicalType = logicalType;
	}

	public boolean isInChoiceData() {
		return isInChoiceData;
	}

	public void setInChoiceData(boolean isInChoiceData) {
		this.isInChoiceData = isInChoiceData;
	}

}
