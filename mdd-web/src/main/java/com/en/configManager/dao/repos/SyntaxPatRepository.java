package com.en.configManager.dao.repos;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.en.configManager.dao.entities.SyntaxPat;
import com.en.configManager.dao.entities.SyntaxPatPK;

@Repository
public interface SyntaxPatRepository extends CrudRepository<SyntaxPat, SyntaxPatPK> {

}
