package com.en.configManager.dao.repos;

 import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.en.configManager.dao.entities.SyntaxEntryField;

@Repository
public interface SyntaxEntryFieldRepository extends CrudRepository<SyntaxEntryField, Long> {

}
