package com.en.configManager.dao.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "STXOPTIONCHOICE")
public class SyntaxOptionChoice implements Comparable<SyntaxOptionChoice>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3223892346770084380L;

	@EmbeddedId
	private SyntaxOptionChoicePK id;

	@Column(nullable = false, length = 132)
	private String expansion;

	@ManyToOne
	@JoinColumns({ @JoinColumn(name = "CODE", referencedColumnName = "CODE", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "CODE_ID", referencedColumnName = "CODE_ID", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "LOOP_ID", referencedColumnName = "LOOP_ID", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "SEQUENCE_ID", referencedColumnName = "SEQUENCE_ID", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "TYPE_IDX", referencedColumnName = "TYPE_IDX", nullable = false, insertable = false, updatable = false) })
	private SyntaxEntryOption stxEntryOption;

	@ManyToOne
	@JoinColumn(name = "TYPE_IDX", nullable = false, insertable = false, updatable = false)
	private SyntaxMessage stxMessage;

	public SyntaxOptionChoicePK getId() {
		return id;
	}

	public void setId(SyntaxOptionChoicePK id) {
		this.id = id;
	}

	public String getExpansion() {
		return expansion;
	}

	public void setExpansion(String expansion) {
		this.expansion = expansion;
	}

	public SyntaxEntryOption getStxEntryOption() {
		return stxEntryOption;
	}

	public void setStxEntryOption(SyntaxEntryOption stxEntryOption) {
		this.stxEntryOption = stxEntryOption;
	}

	public SyntaxMessage getStxMessage() {
		return stxMessage;
	}

	public void setStxMessage(SyntaxMessage stxMessage) {
		this.stxMessage = stxMessage;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((expansion == null) ? 0 : expansion.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SyntaxOptionChoice other = (SyntaxOptionChoice) obj;
		if (expansion == null) {
			if (other.expansion != null)
				return false;
		} else if (!expansion.equals(other.expansion))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public int compareTo(SyntaxOptionChoice o) {

		int compare = id.getSequenceId().compareTo(o.id.getSequenceId());
		if (compare == 0)
			compare = id.getCode().compareTo(o.id.getCode());
		if (compare == 0)
			compare = id.getOptionChoice().compareTo(o.id.getOptionChoice());
		if (compare == 0)
			compare = id.getLoopId().compareTo(o.id.getLoopId());

		return compare;
	}

}
