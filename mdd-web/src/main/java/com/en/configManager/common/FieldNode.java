package com.en.configManager.common;

import java.util.ArrayList;
import java.util.List;

public class FieldNode {



    private String desc;
    private String tag;
    private String idx;
    private String code;
    private String code_id;
    private int image;

    private boolean checked;
    private List <FieldNode>childs;
    private boolean isOptional;





    public FieldNode(String desc) {
        this.desc = desc;
        childs=new ArrayList();
    }

    public FieldNode(String desc, String tag, String idx, String code, String code_id, boolean checked) {
        this.desc = desc;
        this.tag = tag;
        this.idx = idx;
        this.code = code;
        this.code_id = code_id;
        this.checked = checked;
        childs=new ArrayList();

    }

    public boolean isOptional() {
        return isOptional;
    }

    public void setOptional(boolean optional) {
        isOptional = optional;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getIdx() {
        return idx;
    }

    public void setIdx(String idx) {
        this.idx = idx;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode_id() {
        return code_id;
    }

    public void setCode_id(String code_id) {
        this.code_id = code_id;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public List <FieldNode>getChilds() {
        return childs;
    }

    public void setChilds(List childs) {
        this.childs = childs;
    }

    public int getImage() {
        return image;
    }
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.desc;
	}
    public void setImage(int image) {
        this.image = image;
    }
}
