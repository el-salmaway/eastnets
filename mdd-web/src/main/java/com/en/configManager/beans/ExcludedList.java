package com.en.configManager.beans;

import com.en.common.enums.ExcludedListTypesEnum;

public class ExcludedList {
	
	private long id;
	private String name;
	private ExcludedListTypesEnum type;
	
	
	
	
	
	
	public ExcludedList() {
		super();
	}
	public ExcludedList(long id, String name, ExcludedListTypesEnum type) {
		super();
		this.id = id;
		this.name = name;
		this.type = type;
	}
	public ExcludedList(ExcludedListTypesEnum type) {
		super();
		this.type = type;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ExcludedListTypesEnum getType() {
		return type;
	}
	public void setType(ExcludedListTypesEnum type) {
		this.type = type;
	}
	
	

}
