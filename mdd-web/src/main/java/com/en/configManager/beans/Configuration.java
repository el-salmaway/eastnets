package com.en.configManager.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name="Configuration")
@SessionScoped
public class Configuration {
	
	private int id ;
	private String name;
	private String description ;
	private String status ;
	private String bics;
	
	
	
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Configuration(int id, String name, String description, String status, String bics) {
		super();
		this.id=id;
		this.name = name;
		this.description = description;
		this.status = status;
		this.bics = bics;
	}
	
	public Configuration( ) {
		super();
	 
	}
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getBics() {
		return bics;
	}
	public void setBics(String bics) {
		this.bics = bics;
	}
	
	
	
	 @Override
	    public int hashCode() {
	        final int prime = 31;
	        int result = 1;
	        result = prime * result + (id);
	        return result;
	    }

	    @Override
	    public boolean equals(Object obj) {
	        if (this == obj)
	            return true;
	        if (obj == null)
	            return false;
	        if (getClass() != obj.getClass())
	            return false;
	        Configuration other = (Configuration) obj;
	        if (id == 0) {
	            return other.id == 0;
	        } else return id==other.id;
	    }
	
	

}
