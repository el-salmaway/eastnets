package com.en.configManager.beans;

import java.io.Serializable;
import java.util.Objects;

public class Bic implements Serializable,Comparable<Bic> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id ;
	private String code;
	
	
	
	public Bic( ) {
		 
	}
	
	public Bic(int id, String code) {
		super();
		this.id = id;
		this.code = code;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	
	  @Override
	    public boolean equals(Object o) {
	        if (this == o) return true;
	        if (o == null || getClass() != o.getClass()) return false;
	        Bic bic = (Bic) o;
	        return id == bic.id &&
	                Objects.equals(id, bic.id) &&
	                Objects.equals(id, bic.id);
	    }

	    @Override
	    public int hashCode() {
	        return Objects.hash(id,  code);
	    }
	
	    @Override
	    public String toString() {
	        return code;
	    }

	    @Override
	    public int compareTo(Bic o) {
	        return id.compareTo(o.id);
	    }
}
