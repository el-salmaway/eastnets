package com.en.configManager.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.Optional;
import java.util.Random;

import org.primefaces.model.CheckboxTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.en.common.enums.UUMIDFiledsEnum;
import com.en.configManager.beans.Bic;
import com.en.configManager.beans.Configuration;
import com.en.configManager.beans.ExcludedList;
import com.en.configManager.common.FieldNode;
import com.en.configManager.dao.entities.STXGetFieldsView;
import com.en.configManager.dao.entities.SyntaxMessage;
import com.en.configManager.dao.entities.SyntaxVersion;
import com.en.configManager.dao.repos.STXGetFiedsViewRepo;
import com.en.configManager.dao.repos.SyntaxMessageRepository;
import com.en.configManager.dao.repos.SyntaxRepository;

@Service
public class ConfigurationService {

	List<Configuration> configurations;
	List<Bic> Bics;
	List<String> UUMIDChecks;

	
	
	
	
	
	
	
	
	List<ExcludedList> excludedLogTerm;
	List<ExcludedList> excludedTerm;

	@Autowired
	SyntaxRepository versionRepo;

	private final int CB_EMPTY = 1;
	private final int CB_CHECKED = 2;
	private final int CB_FULL = 3;
	private final int CB_SELECT = 4;

	@Autowired
	SyntaxMessageRepository messageRepos;

	@Autowired
	STXGetFiedsViewRepo filedsRepo;

	String saveTag = "";
	String msgDescription = "MT103: Single Customer Credt Transfer";
	String desc;

	public List<SyntaxVersion> getAllSyntaxVersions() {
		List<SyntaxVersion> result = new ArrayList<SyntaxVersion>();
		versionRepo.findAll().forEach(result::add);

		return result;

	}

	public List<SyntaxMessage> getAllFinMSGTypes() {

		List<SyntaxMessage> result = new ArrayList<SyntaxMessage>();
		messageRepos.findAll().forEach(result::add);

		return result;
	}

	public ConfigurationService() {

		configurations = new ArrayList<>();
		configurations.add(new Configuration(1, "config1", "system1Config", "enabled,approved", "PTSABEMM"));
		configurations.add(new Configuration(2, "config2", " ", "enabled,Unapproved", "PTSALUII,"));

		Bics = new ArrayList<Bic>(Arrays.asList(new Bic(1, "BSUIESM0"), new Bic(2, "BSUIDEF0"), new Bic(3, "BSUIAEA0"),
				new Bic(4, "BSEAROB0"), new Bic(5, "BKOAES20"), new Bic(6, "BINVBGS0"), new Bic(7, "BFECGPG0"),
				new Bic(8, "BAGBBRS0"), new Bic(9, "AGRIUAU0"), new Bic(10, "AGRIRER0"), new Bic(11, "AGRIMQM0"),
				new Bic(12, "AGRIGPG0"), new Bic(13, "AGRIEGC0"), new Bic(14, "AGRIGB20"), new Bic(15, "BSUIDZA0"),
				new Bic(16, "CRLYFR20"), new Bic(17, "BPPNIT20"), new Bic(18, "ISAEGB20"), new Bic(19, "ISAECH20"),
				new Bic(20, "CVMCBEB0"), new Bic(21, "AGRIHKH0"), new Bic(22, "AGRIESM0"), new Bic(23, "AGRIITM0"),
				new Bic(24, "ISAEIE20"), new Bic(25, "AGRLFR20"), new Bic(26, "BSUIJPJ0"), new Bic(27, "ZYBHJPJ0"),
				new Bic(28, "BSUIAU20"), new Bic(29, "CRLYCNS0"), new Bic(30, "CRLYHKH0")));

		UUMIDChecks = new ArrayList<String>(
				Arrays.asList(UUMIDFiledsEnum.CORRESPONDENT_BIC.toString(), UUMIDFiledsEnum.DIRECTION.toString(),
						UUMIDFiledsEnum.MESSAGE_TYPE.toString(), UUMIDFiledsEnum.RECEIVER.toString(),
						UUMIDFiledsEnum.REFERENCE.toString(), UUMIDFiledsEnum.REQUEST_TYPE.toString(),
						UUMIDFiledsEnum.REQUESTOR.toString(), UUMIDFiledsEnum.RESPONDER.toString(),
						UUMIDFiledsEnum.SENDER.toString(), UUMIDFiledsEnum.UNIQUE_MESSAGE_IDENTIFIER.toString()));

	}

	public List<Bic> getBics() {
		return this.Bics;
	}

	public List<String> getUUMIDFileds() {
		return this.UUMIDChecks;
	}

	public List<Configuration> getConfigurations() {
		return configurations;
	}

	public List<Configuration> getConfigurations(int size) {

		if (size > configurations.size()) {
			Random rand = new Random();

			List<Configuration> randomList = new ArrayList<>();
			for (int i = 0; i < size; i++) {
				int randomIndex = rand.nextInt(configurations.size());
				randomList.add(configurations.get(randomIndex));
			}

			return randomList;
		}

		else {
			return configurations.subList(0, size);
		}

	}
	public TreeNode test() {
		
		Optional<SyntaxMessage >msg=messageRepos.findById(new Long(1));
		if(msg.isPresent())return this.buildConfigurationTree(msg.get());
		else return null;
		
	}
	public TreeNode buildConfigurationTree(SyntaxMessage msg) {
		TreeNode root = new CheckboxTreeNode(new FieldNode("Files"), null);

		List<FieldNode> tree = new ArrayList<>();

		FieldNode fldNode = null;

		List<STXGetFieldsView> fields = filedsRepo.getfields(msg.getType());
		ListIterator<STXGetFieldsView> it = fields.listIterator();
		STXGetFieldsView field = (it.hasNext()) ? it.next() : null;

		while (it.hasNext()) {
			saveTag = field.getTag() + field.getEntryOption();
			desc = field.getTag();
			desc += (field.getLoopId() > 0) ? "(R)" : "";

			if (field.getEntryAlternate() != null) {

				if (desc.equals("F59") || desc.equals("F59(R)")) {
					// Multiple choide node. Create main entry
					fldNode = new FieldNode(desc);
					fldNode.setImage(CB_EMPTY);
					
					tree.add(fldNode);
					TreeNode fldTreeNode = new CheckboxTreeNode(fldNode, root);

					Long fldCode = field.getCode();
					Long fldCodeId = field.getCodeId();
					boolean bDone = false;

					do {
						FieldNode option = new FieldNode(field.getTag() + ": " + field.getExpansion());
						option.setImage(CB_EMPTY);
						
						fldNode.getChilds().add(option);
						TreeNode optionTreeNode = new CheckboxTreeNode(option, fldTreeNode);

						FieldNode optionChild = null;

						if (option.getDesc().equals("F59: Beneficiary Customer-Account - Name and Address")
								|| option.getDesc().equals("59: Beneficiary - Name and Address")
								|| option.getDesc().equals("F59: Debtor-Name and Address")
								|| option.getDesc().equals("F59: Payee-Name and Address")) {
							optionChild = new FieldNode("Account");
							optionChild.setImage(CB_EMPTY);
							option.getChilds().add(optionChild);
							 new CheckboxTreeNode(optionChild, optionTreeNode);

							optionChild = new FieldNode("Name and Address");
							optionChild.setImage(CB_EMPTY);
							option.getChilds().add(optionChild);
							new CheckboxTreeNode(optionChild, optionTreeNode);

						}
						// Beneficiary Customer-Account - Identifier Code

						else if (option.getDesc().equals("F59A: Beneficiary Customer-Account - Identifier Code")
								|| option.getDesc().equals("F59A: Beneficiary - BIC")
								|| option.getDesc().equals("F59A: Debtor - BIC")) {

							optionChild = new FieldNode("Account");
							optionChild.setImage(CB_EMPTY);
							
							option.getChilds().add(optionChild);
							 new CheckboxTreeNode(optionChild, optionTreeNode);
							 
							 
							optionChild = new FieldNode("BIC");
							optionChild.setImage(CB_EMPTY);
							
							option.getChilds().add(optionChild);
							 new CheckboxTreeNode(optionChild, optionTreeNode);

						} else if (option.getDesc()
								.equals("F59F: Beneficiary Customer-Account - Number/Name and Address")
								|| option.getDesc().equals("F59F: Payee - Number/Name and Address")) {
							optionChild = new FieldNode("Account");
							optionChild.setImage(CB_EMPTY);
							
							option.getChilds().add(optionChild);
							new CheckboxTreeNode(optionChild, optionTreeNode);
							
							optionChild = new FieldNode("Number / Name and Address");
							optionChild.setImage(CB_EMPTY);
							
							option.getChilds().add(optionChild);
							new CheckboxTreeNode(optionChild, optionTreeNode);
						}

						if (field.getIsOptional() != 0) {
							option.setOptional(true);
							fldNode.setOptional(true);
							option.getChilds().stream().forEach(e -> e.setOptional(true));

						}
						option.setTag("opt" + field.getEntryOption());
						field = it.next();
						if (!it.hasNext()) {
							bDone = true;
						} else if (field.getCode() != fldCode || field.getCodeId() != fldCodeId) {
							bDone = true;
						}

					} while (!bDone);
					//field = it.previous();

				}

				else if (desc.equals("F50") && (msgDescription.equals("MT103: Single Customer Credt Transfer"))) {
					fldNode = new FieldNode(desc);
					fldNode.setImage(CB_EMPTY);
					
					tree.add(fldNode);
					TreeNode fldTreeNode=new CheckboxTreeNode(fldNode, root);
					
					Long fldCode = field.getCode();
					Long fldCodeId = field.getCodeId();
					boolean bDone = false;

					do {
						FieldNode option = new FieldNode(field.getTag() + field.getEntryOption() + ": " + field.getExpansion());
						option.setImage(CB_EMPTY);
						fldNode.getChilds().add(option);
						TreeNode optionTreeNode=new CheckboxTreeNode(option, fldTreeNode);

						
						
						FieldNode optionChild = null;

						if (option.getDesc().equals("F50A: Ordering Customer - BIC")
								|| option.getDesc().equals("F50A: Ordering Customer-Account - Identifier Code")) {
							optionChild = new FieldNode("Account");
							optionChild.setImage(CB_EMPTY);
							option.getChilds().add(optionChild);
							new CheckboxTreeNode(optionChild, optionTreeNode);
							
							
							optionChild = new FieldNode("BIC");
							optionChild.setImage(CB_EMPTY);
							option.getChilds().add(optionChild);
							new CheckboxTreeNode(optionChild, optionTreeNode);
						} else if (option.getDesc().equals("F50F: Ordering Customer - ID") || option.getDesc()
								.equals("F50F: Ordering Customer-Party Identifier - Number/Name and Address")) {
							optionChild = new FieldNode("Party Identifier");
							optionChild.setImage(CB_EMPTY);
							option.getChilds().add(optionChild);
							new CheckboxTreeNode(optionChild, optionTreeNode);
							

							optionChild = new FieldNode("Name and Address");
							optionChild.setImage(CB_EMPTY);
							option.getChilds().add(optionChild);
							new CheckboxTreeNode(optionChild, optionTreeNode);
							

						} else if (option.getDesc().equals("F50K: Ordering Customer-Name and Address")
								|| option.getDesc().equals("F50 K: Ordering Customer-Account - Name and Address")) {
							optionChild = new FieldNode("Account");
							optionChild.setImage(CB_EMPTY);
							option.getChilds().add(optionChild);
							new CheckboxTreeNode(optionChild, optionTreeNode);

							optionChild = new FieldNode("Name and Address");
							optionChild.setImage(CB_EMPTY);
							option.getChilds().add(optionChild);
							new CheckboxTreeNode(optionChild, optionTreeNode);

						}

						if (field.getIsOptional() != 0) {
							option.setOptional(true);
							fldNode.setOptional(true);
							option.getChilds().stream().forEach(e -> e.setOptional(true));

						}
						option.setTag("opt" + field.getEntryOption());

						field = it.next();
						if (!it.hasNext()) {
							bDone = true;
						} else if (field.getCode() != fldCode || field.getCodeId() != fldCodeId) {
							bDone = true;
						}

					} while (!bDone);
					//field = it.previous();

				} else {
					fldNode = new FieldNode(field.getEntryAlternate());
					fldNode.setImage(CB_EMPTY);
					fldNode.setOptional(field.getIsOptional() != 0);
					
					tree.add(fldNode);
					TreeNode fldTreeNode= new CheckboxTreeNode(fldNode, root);
					
					String alternate = field.getEntryAlternate();
					String choice = field.getEntryAlternateChoice();

					boolean bDone = false;
					do {
						FieldNode alt = new FieldNode(field.getTag() + field.getExpansion());
						alt.setTag("alt" + alternate + "." + choice);
						alt.setImage((field.getMesgType() != null && field.getConfigId() != null) ? CB_SELECT : CB_EMPTY);
						alt.setOptional(field.getIsOptional() != 0);
						fldNode.getChilds().add(alt);
						new CheckboxTreeNode(alt, fldTreeNode);

						
						
						
						boolean bChoiceDone = false;

						do {
							field = it.next();
							if (!it.hasNext())
								bChoiceDone = true;
							else if (field.getEntryAlternateChoice() == null)
								bChoiceDone = true;
							else if (!field.getEntryAlternateChoice().equals(choice))
								bChoiceDone = true;

						} while (!bChoiceDone);

						if (it.hasNext())
							bDone = true;
						else if (field.getEntryAlternate() == null)
							bDone = true;
						else if (!field.getEntryAlternate().equals(alternate))
							bDone = true;

					} while (!bDone);
				//	field = it.previous();

				}
			}
			else if (field.getEntryOption().equals("0")) {

				if (desc.equals("F32A") || desc.equals("F32A (R)")) {

					fldNode = new FieldNode(desc);
					fldNode.setImage(CB_EMPTY);

					tree.add(fldNode);
					TreeNode fldTreeNode= new CheckboxTreeNode(fldNode, root);
	
					
					

					FieldNode option = new FieldNode("Val Date");
					option.setImage(CB_EMPTY);
					fldNode.getChilds().add(option);
					new CheckboxTreeNode(option, fldTreeNode);
					
					
					option = new FieldNode("Val Currency");
					option.setImage(CB_EMPTY);
					fldNode.getChilds().add(option);
					new CheckboxTreeNode(option, fldTreeNode);
					

					option = new FieldNode("Interbnk Settld Amount");
					option.setImage(CB_EMPTY);
					fldNode.getChilds().add(option);
					new CheckboxTreeNode(option, fldTreeNode);

				} else if (desc.equals("F59") || desc.equals("F59 (R)")) {
					// Multiple choide node. Create main entry
					fldNode = new FieldNode(desc);
					fldNode.setImage(CB_EMPTY);
					
					tree.add(fldNode);
					TreeNode fldTreeNode= new CheckboxTreeNode(fldNode, root);
					
					
					Long fldCode = field.getCode();
					Long fldCodeId = field.getCodeId();
					boolean bDone = false;
					do {
						FieldNode option = new FieldNode(field.getTag() + ": " + field.getExpansion());
						option.setImage(CB_EMPTY);
						fldNode.getChilds().add(option);
						TreeNode optionTreeNode= new CheckboxTreeNode(option, fldTreeNode);

						FieldNode optionChild = null;
						if (option.getDesc().equals("F59 : Payee - Name and Address")
								|| option.getDesc().equals("F59: Beneficiary - Name and Address")
								|| option.getDesc().equals("F59: Benefic'y (before amndmt)-Nm and Add")
								|| option.getDesc().equals("F59: Second Beneficiary")
								|| option.getDesc().equals("F59: Enquired Party - Name and Address")) {

							optionChild = new FieldNode("Account");
							optionChild.setImage(CB_EMPTY);
							option.getChilds().add(optionChild);
							 new CheckboxTreeNode(optionChild, optionTreeNode);
							

							optionChild = new FieldNode("Name and Address");
							optionChild.setImage(CB_EMPTY);
							option.getChilds().add(optionChild);
							 new CheckboxTreeNode(optionChild, optionTreeNode);

						}

						if (field.getIsOptional() != 0) {
							option.setOptional(true);
							fldNode.setOptional(true);
							option.getChilds().stream().forEach(e -> e.setOptional(true));

						}
						option.setTag("opt" + field.getEntryOption());
						field = it.next();
						if (!it.hasNext()) {
							bDone = true;
						} else if (field.getCode() != fldCode || field.getCodeId() != fldCodeId) {
							bDone = true;
						}

					} while (!bDone);
					//field = it.previous();

				} else {

					fldNode = new FieldNode(desc + ": " + field.getExpansion());
					fldNode.setOptional(field.getIsOptional() != 0);
					fldNode.setImage(CB_EMPTY);
					tree.add(fldNode);
					 new CheckboxTreeNode(fldNode, root);


				}

			}
			else {

				if (desc.equals("F50") && (msgDescription.equals("MT103: Single Customer Credt Transfer"))) {
					fldNode = new FieldNode(desc);
					fldNode.setImage(CB_EMPTY);
					
					tree.add(fldNode);
					TreeNode fldTreeNode= new CheckboxTreeNode(fldNode, root);
					
					
					
					Long fldCode = field.getCode();
					Long fldCodeId = field.getCodeId();
					boolean bDone = false;

					do {
						FieldNode option = new FieldNode(
								field.getTag() + field.getEntryOption() + ": " + field.getExpansion());
						option.setImage(CB_EMPTY);
						fldNode.getChilds().add(option);
						TreeNode optionTreeNode= new CheckboxTreeNode(option, fldTreeNode);
						FieldNode optionChild = null;

						if (option.getDesc().equals("F50A: Ordering Customer - BIC")
								|| option.getDesc().equals("F50A: Ordering Customer-Account - Identifier Code")) {// F50A:
																													// Ordering
																													// Customer-Account
																													// -
																													// Identifier
																													// Code
							optionChild = new FieldNode("Account");
							optionChild.setImage(CB_EMPTY);
							option.getChilds().add(optionChild);
							new CheckboxTreeNode(optionChild, optionTreeNode);
							

							optionChild = new FieldNode("BIC");
							optionChild.setImage(CB_EMPTY);
							option.getChilds().add(optionChild);
							new CheckboxTreeNode(optionChild, optionTreeNode);

						} else if (option.getDesc().equals("F50F: Ordering Customer - ID") || option.getDesc()
								.equals("F50F: Ordering Customer-Party Identifier - Number/Name and Address")) {
							optionChild = new FieldNode("Party Identifier");
							optionChild.setImage(CB_EMPTY);
							option.getChilds().add(optionChild);
							new CheckboxTreeNode(optionChild, optionTreeNode);
							
							
							optionChild = new FieldNode("Name and Address");
							optionChild.setImage(CB_EMPTY);
							option.getChilds().add(optionChild);
							new CheckboxTreeNode(optionChild, optionTreeNode);

						} else if (option.getDesc().equals("F50K: Ordering Customer-Name and Address")
								|| option.getDesc().equals("F50K: Ordering Customer-Account - Name and Address")) {
							optionChild = new FieldNode("Account");
							optionChild.setImage(CB_EMPTY);
							option.getChilds().add(optionChild);
							new CheckboxTreeNode(optionChild, optionTreeNode);

							optionChild = new FieldNode("Name and Address");
							optionChild.setImage(CB_EMPTY);
							option.getChilds().add(optionChild);
							new CheckboxTreeNode(optionChild, optionTreeNode);

						}

						if (field.getIsOptional() != 0) {
							option.setOptional(true);
							fldNode.setOptional(true);
							option.getChilds().stream().forEach(e -> e.setOptional(true));

						}
						option.setTag("opt" + field.getEntryOption());
						field = it.next();
						if (!it.hasNext())
							bDone = true;
						else if (field.getCode() != fldCode || field.getCodeId() != fldCodeId)
							bDone = true;

					} while (!bDone);
					// it.previous();
					// field = it.previous();

				} else {
					fldNode = new FieldNode(desc);
					fldNode.setOptional(field.getIsOptional() != 0);
					
					tree.add(fldNode);
					TreeNode fldTreeNode= new CheckboxTreeNode(fldNode, root);
					Long fldCode = field.getCode();
					Long fldCodeId = field.getCodeId();
					boolean bDone = false;
					do {
						FieldNode option = new FieldNode(field.getEntryOption() + field.getExpansion());
						option.setOptional(field.getIsOptional() != 0);
						option.setTag("opt" + field.getTag());
						option.setChecked((field.getMesgType() != null && field.getConfigId() != null));
						fldNode.getChilds().add(option);
						 new CheckboxTreeNode(option, fldTreeNode);
						
						field = it.next();
						if (!it.hasNext())
							bDone = true;
						else if (field.getCode() != fldCode || field.getCodeId() != fldCodeId)
							bDone = true;

					} while (!bDone);

					// field = it.previous();

				}

			}

			fldNode.setChecked((field.getMesgType() != null && field.getConfigId() != null));
			fldNode.setTag("fld" + field.getIdx() + ".");

			int loopNext = 0;

			do {

				if (it.hasNext()) {
					if (saveTag.equals(field.getTag() + field.getEntryOption())) {
						field = it.next();
					} else {
						saveTag = field.getTag() + field.getEntryOption();

						loopNext = 1;

					}
				} else {
					loopNext = 1;
				}

			} while (loopNext == 0);

		}
return root;
	}
}
