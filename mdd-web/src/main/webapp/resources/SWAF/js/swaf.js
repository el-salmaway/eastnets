// SWAF javascript functions

/* Removes the browser navigation history */
window.history.forward();

/* Returns the specified element */
function $obj(x)
{
	return document.getElementById(x);
}

if (self === top) {
    var antiClickjack = document.getElementById("antiClickjack");
    antiClickjack.parentNode.removeChild(antiClickjack);
} else {
    top.location = self.location;
}

function getInternetExplorerVersion() {
    var rv = -1;
    if (navigator.appName == 'Microsoft Internet Explorer')
    {
        var ua = navigator.userAgent;
        var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
        if (re.exec(ua) != null)
            rv = parseFloat( RegExp.$1 );
    }
    else if (navigator.appName == 'Netscape')
    {
        var ua = navigator.userAgent;
        var re  = new RegExp("Trident/.*rv:([0-9]{1,}[\.0-9]{0,})");
        if (re.exec(ua) != null)
            rv = parseFloat( RegExp.$1 );
    }
    return rv;
}

//Create method trim for IE8
if(typeof String.prototype.trim !== 'function') {
    String.prototype.trim = function() {
        return this.replace(/^\s+|\s+$/g, '');
    }
}

jQuery(document).ready(function() {
	swafonload();
});

/* Sets the active transaction flag. True means that the user 
   is editing something in a form. */
function setActiveTransactionFlag(flag)
{
	if (window.document.forms['TrxHiddenForm'] != null)
	{
		window.document.forms['TrxHiddenForm'].elements['TrxHiddenForm:activeTrx'].value = flag;
	}
}

/* Returns the active transaction flag. True means that the user
   is editing something in a form and/or something is modified 
   at the server level. */
function isActiveTransaction()
{
	if (window.document.forms['TrxHiddenForm'] != null)
	{
		if (window.document.forms['TrxHiddenForm'].elements['TrxHiddenForm:activeTrx'].value == 'true') 
		{
			return true;
		}
		
		if (window.document.forms['TrxHiddenForm'].elements['TrxHiddenForm:activeServerTrx'].value == 'true') 
		{
			return true;
		}
	}
	return false;
}

/* Returns the active transaction flag. True means that the user
   is editing something in a form. */
function isActiveNavigatorTransaction()
{
	if (window.document.forms['TrxHiddenForm'] != null)
	{
		if (window.document.forms['TrxHiddenForm'].elements['TrxHiddenForm:activeTrx'].value == 'true') 
		{
			return true;
		}
	}
	return false;
}

/* Returns the active transaction flag. True means that the user
   is editing something in a form. */
function rollbackActiveNavigatorTransaction(message)
{
	if (isActiveNavigatorTransaction() == true)
	{
		if (confirmationDlg(message)) 
		{
			return true
		}
		else
		{
			return false;
		}
	}
	return true;
}

/* If an active transacion is 'running', this function asks the
   user if he wants to cancel the transaction or not. */
function rollbackActiveTransaction(message)
{
	if (isActiveTransaction() == true)
	{
		if (confirmationDlg(message)) 
		{
			return true
		}
		else
		{
			return false;
		}
	}
	return true;
}

/* Sets the status bar message */
function updateStatusBar() 
{ 
	if (window.document.forms['statusBarForm'] != null && document.forms['SBHiddenForm'] != null)
	{
		window.document.getElementById('statusDescription').innerHTML = document.forms['SBHiddenForm'].elements['SBHiddenForm:statusDescription'].value; 
	}
}

/* Handle the 'enter' key pressed. Click on the 
   specified 'command button'. */
function submitEnter(commandId, e)
{
	var keycode;
	if (window.event) 
		keycode = window.event.keyCode;
	else if (e) 
		keycode = e.which;
	else 
		return true;
        
	if (keycode == 13) 
	{
		if (commandId == ''){
			b= 'btnTargetPageHidden';
		}

		if (commandId != '') jQuery( "input[type='submit'][name$=" + commandId + "]").click();
		return false;
	} 
	else
		return true;
}

function findTableIdEndingBy(id) {
	return jQuery("div [id$='" + id + "']").attr('id'); 	
}

/*
 * FOR TEST PURPOSE !!!
 */
function doEdit(displayId, editorId, e)
{
	if (document.getElementById(displayId) != null && document.getElementById(editorId) != null) 
	{
		displayComponent = document.getElementById(displayId);
		editorComponent = document.getElementById(editorId);

		displayComponent.style.display = 'none';

		editorComponent.style.display = 'inline';
		editorComponent.style.background = 'white';
		editorComponent.style.border = '2px solid #7F9DB9';
		editorComponent.style.height = '85%';
		editorComponent.select();
		editorComponent.focus();

		return true;
	}
	return false;
}

/*
 * FOR TEST PURPOSE !!!
 */
function cancelEdit(displayId, editorId, e)
{
	if (document.getElementById(displayId) != null && document.getElementById(editorId) != null) 
	{
		displayComponent = document.getElementById(displayId);
		editorComponent = document.getElementById(editorId);

		displayComponent.style.display = 'inline';
		editorComponent.style.display = 'none';

		return true;
	}
	return false;
}

/*
 * Simulates a click on a specified button.
 */
function clickButton(commandId)
{
	jQuery( "input[type='submit'][name$=" + commandId + "]").click();
	return true;
}

/* Returns the number of selected check box
   in the specified table. */
function getNumberSelectedTableCheckbox(tableId)
{
	var selectedNbr = 0;

	table = document.getElementById(tableId);
	if (table == null) return selectedNbr;
	
	inputs = table.getElementsByTagName('input');
	for (i = 0; i < inputs.length; i++)
	{
		if (inputs[i].type == 'checkbox')
	    {
	    	if (inputs[i].checked == true) {
	    		if (typeof jQuery(inputs[i]).attr('id') != 'undefined' && jQuery(inputs[i]).attr('id').indexOf('inputSwitch') >= 0) {
	    			continue;
	    		}
	    		selectedNbr ++;
	    	}
		}
	}

	return selectedNbr;
}

/* Unselect all others checkbox. */
function unselectAllOthers(tableId,checkBox)
{
	table = document.getElementById(tableId);
	if (table == null) return selectedNbr;
	
	inputs = table.getElementsByTagName('input');
	for (i = 0; i < inputs.length; i++)
	{
	    if (inputs[i].type == 'checkbox')
	    {
	    	if (inputs[i]!=checkBox)
	    	{
	    	inputs[i].checked = false
	    	}
		}
	}
}



/* Checks if the specified table contains one and
   only one selected checkbox. */
function isOnlyOneTableCheckboxSelected(tableId)
{
	var selectedNbr = getNumberSelectedTableCheckbox(tableId);
	
	if (selectedNbr == 1) return true;
	else return false;
}

/* Checks if the specified table contains one or
   more selected checkbox. */
function isOneOrMoreTableCheckboxSelected(tableId)
{
	var selectedNbr = getNumberSelectedTableCheckbox(tableId);
	
	if (selectedNbr > 0) return true;
	else return false;
}

/* Displays an information message. This information message is
   always composed by a 'message' and can be optionally composed
   by a user 'recommended action'. */
function informationDlg(pm_message, pm_recommendedAction)
{
	var formattedMessage = pm_message;
	var recommendedAction = pm_recommendedAction
	
	formattedMessage = formattedMessage.replace("[CR]", "\n");
	if (recommendedAction != null && recommendedAction.length > 0)
	{
		recommendedAction = recommendedAction.replace("[CR]", "\n");
		formattedMessage += "\n\n";
		formattedMessage += recommendedAction;
	}
	
	alert(formattedMessage);
}

/* Displays an information message. This information message is
   always composed by a 'message' and can be optionally composed
   by a user 'recommended action'. */
function recommendedActionDlg(pm_message, pm_recommendedActionTitle, pm_recommendedAction)
{
	var message = pm_message;
	var recommendedAction = pm_recommendedActionTitle + "\n" + pm_recommendedAction;
	informationDlg(message, recommendedAction);
}

/* Displays a confirmation message. */
function confirmationDlg(pm_message)
{
	var text = pm_message
	return confirm(text.replace("[CR]", "\n"));
}

/* Called when a jsp page is loaded. Initializes the status bar and checks 
   if an error dialog box should be displayed. */
function swafonload() 
{ 
	updateStatusBar(); 

	if ($obj('SBHiddenForm:serverBoxText') != null && $obj('SBHiddenForm:serverBoxText').value != '') 
	{
		informationDlg($obj('SBHiddenForm:serverBoxText').value, $obj('SBHiddenForm:serverBoxAction').value);

		$obj('SBHiddenForm:serverBoxText').value = '';
		$obj('SBHiddenForm:serverBoxAction').value = '';
	}
	
	applyCustomCode();
} 

/* 
 * FOR TEST PURPOSE !!!	
 */
function checkIntegerOld(pm_field, pm_minVal, pm_maxVal)
{
	if (privateCheckInteger(pm_field, pm_minVal, pm_maxVal) == false)
	{
		pm_field.value = 0;
		pm_field.select();
		pm_field.focus();
		return false;
	}
	else
	{
		return true;
	}
}

/* Checks in the specified field contains a valid numerci 
   value in the specified range. The old value is used in case
   of invalid specified value.
 */
function checkInteger(pm_field, pm_minVal, pm_maxVal, pm_oldValue)
{
	if (pm_minVal <= 0)
		pm_minVal = 1;
	if (privateCheckInteger(pm_field, pm_minVal, pm_maxVal) == false)
	{
		pm_field.value = pm_oldValue;
		pm_field.select();
		pm_field.focus();
		return false;
	}
	else
	{
		return true;
	}
}

/* Utily function used for numeric range value check. */
function privateCheckInteger(pm_field, pm_minVal, pm_maxVal)
{
	var chkVal = pm_field.value;
	if (chkVal == "") 
	{
		return false;
	}

	// only allow 0-9 be entered
	if (isNum(chkVal) == false)
	{
		return false;
	}

	// check the minimum and maximum
	var prsVal = parseInt(chkVal);
	if (!(prsVal >= pm_minVal && prsVal <= pm_maxVal))
	{
		return false;
	}
}

/* Represents all valid numeric values. */
var VALID_NUMBER = '0123456789';
/* Represents all valid lowercase alphabetic values. */
var VALID_ALPHA_LC = 'abcdefghijklmnopqrstuvwxyz';
/* Represents all valid uppercase alphabetic values. */
var VALID_ALPHA_UC = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

/* Checks if the specified source value match the specified 'pattern'. */
function isValid(pm_source, pm_validValue) 
{
	if (pm_source == "") return true;
	for (i=0; i < pm_source.length; i++) 
	{
		if (pm_validValue.indexOf(pm_source.charAt(i), 0) == -1) return false;
	}
	return true;
}

/* Checks if the specified source value is a number. */
function isNum(pm_source) 
{
	return isValid(pm_source, VALID_NUMBER);
}

/* Checks if the specified source value is a lowercase alphabetic string. */
function isLower(pm_source) 
{
	return isValid(pm_source, VALID_ALPHA_LC);
}

/* Checks if the specified source value is a lowercase alphabetic string. */
function isUpper(pm_source) 
{
	return isValid(pm_source, VALID_ALPHA_UC);
}

/* Checks if the specified source value is an uppercase alphabetic string. */
function isAlpha(pm_source) 
{
	return isValid(pm_source, VALID_ALPHA_LC + VALID_ALPHA_UC);
}

/* Checks if the specified source value is an alphanumeric (lowercase & uppercase) value. */
function isAlphanum(pm_source) 
{
	return isValid(pm_source, VALID_ALPHA_LC + VALID_ALPHA_UC + VALID_NUMBER);
}

/* Sets the specified value to the specified field. */
function setNewValue(pm_field, pm_newValue)
{
	if (document.getElementById(pm_field) != null)
	{
		document.getElementById(pm_field).value = pm_newValue;
		return true;
	}
	
	return false;
}

/* Activates the specified text field. By activating, we mean selecting the contains 
   and setting the focus. */
function activateTextField(pm_field)
{
	if (document.getElementById(pm_field) != null)
	{
		field = document.getElementById(pm_field);
	
		field.select();
		field.focus();
	}
	
	return true;
}

function storeCaret(ftext) 
{
	if (document.getElementById('settingsEditorForm:homepage_business:mailTemplateTab:tab_mailTemplates_business:sub_mailTemplates_details_business:mailBody') != null)
	{
		field = document.getElementById('settingsEditorForm:homepage_business:mailTemplateTab:tab_mailTemplates_business:sub_mailTemplates_details_business:mailBody');
		//IE
		if(field.createTextRange)
		{
			field.caretPos = document.selection.createRange().duplicate();	
		}	
		//FFox
		if (field.setSelectionRange) 
		{
			var selection = window.getSelection();
			var CaretPos = field.selectionStart;
			field.caretPos = CaretPos;
		}
	}	
}

function insertsmilie(smilieface) 
{				
	if (document.getElementById('settingsEditorForm:homepage_business:mailTemplateTab:tab_mailTemplates_business:sub_mailTemplates_details_business:mailBody') != null)
	{
		field = document.getElementById('settingsEditorForm:homepage_business:mailTemplateTab:tab_mailTemplates_business:sub_mailTemplates_details_business:mailBody');
		var fieldVal = field.value;
	    var val = document.getElementById('settingsEditorForm:homepage_business:mailTemplateTab:tab_mailTemplates_business:sub_mailTemplates_details_business:mailParamCbx').value;
		if (field.setSelectionRange && field.caretPos) 
		{
			var caretPos = field.caretPos;	
			var old = fieldVal.substring(0,caretPos); 
			var news = fieldVal.substring(caretPos, fieldVal.length);  
			field.value =old+val+news;
			field.focus();
		}
		if(navigator.userAgent.indexOf('MSIE')!=0)
		{
			var caretPos1 = field.caretPos;
			caretPos1.text = val;	
		}
	}	
	
}

function clearForm(oForm) {
	var frm_elements = oForm.elements;
	
	for (i = 0; i < frm_elements.length; i++)
	{
	    field_type = frm_elements[i].type.toLowerCase();
	    switch (field_type)
	    {
	    case "text":
	    case "password":
	    case "textarea":
	    case "hidden":
	        frm_elements[i].value = "";
	        break;
	    case "radio":
	    case "checkbox":
	        if (frm_elements[i].checked)
	        {
	            frm_elements[i].checked = false;
	        }
	        break;
	    case "select-one":
	    case "select-multi":
	        frm_elements[i].selectedIndex = -1;
	        break;
	    default:
	        break;
	    }
	}
}

function applyCustomCode() {
	// nothing to do (could be overridden)
}

function changePosition(button, dialog) {
	var x = button.offset().left;
	var y = button.offset().top;
	var width = dialog.width();
	var height = dialog.height() / 2;
	var padding = 10;
	dialog.offset({ top: y - height, left: x - width - padding });
}

$(function () {
	$('.sidebar-navigation > li').on('mouseover', function () {
		var $item = $(this), $tooltip= $('> .layout-menu-tooltip', $item);
		var itemPosition = $item.position();
		$tooltip.css({
			top: itemPosition.top + 10,
			left: itemPosition.left + 70
		})
	})
})