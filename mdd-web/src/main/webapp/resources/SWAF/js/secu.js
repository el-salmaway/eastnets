function contains(password, allowedChars, minNumber) {
    var counter = 0;
    for (i = 0; i < password.length; i++) {
        var char = password.charAt(i);
        if (allowedChars.indexOf(char) >= 0) {
            counter = counter + 1;
        }
    }
    if (counter >= minNumber) {
        return true;
    }
    return false;
}

function checkPasswordPolicy(password) {
    var minimumNumerics = document.getElementById('passwordMinimumNumerics').value;
    var minimumUpperChars = document.getElementById('passwordMinimumUpperChars').value;
    var minimumLowerChars = document.getElementById('passwordMinimumLowerChars').value;
    var minimumSpecialChars = document.getElementById('passwordMinimumSpecialChars').value;

    var uppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var lowercase = "abcdefghijklmnopqrstuvwxyz";
    var digits = "0123456789";
    var splChars = "!@#$%&*()";

    var upperFlag = contains(password, uppercase, minimumUpperChars);
    var lowerFlag = contains(password, lowercase, minimumLowerChars);
    var digitsFlag = contains(password, digits, minimumNumerics);
    var specialFlag = contains(password, splChars, minimumSpecialChars);
    if (upperFlag && lowerFlag && digitsFlag && specialFlag)
        return true;
    else
        return false;
}

function getCipher(path) {
    var salt = document.getElementById(path + "salt");
    var pwdOld = document.getElementById(path + "oldPassword");
    var pwdNew = document.getElementById(path + "newPassword");
    var pwdConfirm = document.getElementById(path + "confirmNewPassword");
    var md5Password = document.getElementById(path + "isMD5Password");

    if (!pwdOld || !pwdNew || !pwdConfirm || !md5Password) return;
    if (pwdOld.textLength <= 0 || pwdNew.textLength <= 0 || pwdConfirm.textLength <= 0) return;

    var minAllowedLength = document.getElementById('passwordMinimumLength').value;
    if (!(pwdNew.value.length >= minAllowedLength)) {
        passwordNotMeetLengthPolicy();
    }
    var maxAllowedLength = document.getElementById('passwordMaximumLength').value;
    if (!(pwdNew.value.length <= maxAllowedLength)) {
        passwordNotMeetMaxLengthPolicy();
    }
    var isPasswordPolicyEnabled = document.getElementById('passwordPolicyEnabled').value;
    if (isPasswordPolicyEnabled === "true") {
        var isValidNewPassword = checkPasswordPolicy(pwdNew.value);
        if (!isValidNewPassword) {
            passwordNotMeetPolicy();
        }
    }

    var hashedPass = sjcl.hash.sha256.hash(pwdNew.value);
    pwdNew.value = hashedPass;

    hashedPass = sjcl.hash.sha256.hash(pwdConfirm.value);
    pwdConfirm.value = hashedPass;

    if (md5Password.value == "false") {
        hashedPass = sjcl.hash.sha256.hash(pwdOld.value);
        pwdOld.value = hashedPass;

    } else {
        pwdOld.value = getEncrypted(pwdOld.value, salt.value);
    }
}

function checkPasswordPolicy2(path) {
    var pwdOld = document.getElementById(path + "oldPassword");
    var pwdNew = document.getElementById(path + "newPassword");
    var pwdConfirm = document.getElementById(path + "confirmNewPassword");
    var md5Password = document.getElementById(path + "isMD5Password");

    if (!pwdOld || !pwdNew || !pwdConfirm || !md5Password) return;
    if (pwdOld.textLength <= 0 || pwdNew.textLength <= 0 || pwdConfirm.textLength <= 0) return;

    var minAllowedLength = document.getElementById('passwordMinimumLength').value;
    if (!(pwdNew.value.length >= minAllowedLength)) {
        passwordNotMeetLengthPolicy();
    }
    var maxAllowedLength = document.getElementById('passwordMaximumLength').value;
    if (!(pwdNew.value.length <= maxAllowedLength)) {
        passwordNotMeetMaxLengthPolicy();
    }
    var isPasswordPolicyEnabled = document.getElementById('passwordPolicyEnabled').value;
    if (isPasswordPolicyEnabled === "true") {
        var isValidNewPassword = checkPasswordPolicy(pwdNew.value);
        if (!isValidNewPassword) {
            passwordNotMeetPolicy();
        }
    }

}
