function applyCustomCode() {
    Cookies.remove('freya_expandeditems', {path: '/'});

    processAuthChange();
    setFocus();
}

$(window).on("beforeunload", function(e) {
    window.history.forward();
});

function hideObject(object_id) {
    let elem = document.getElementById(object_id);
    elem.style.visibility = 'hidden';
}

function unhideObject(object_id) {
    let elem = document.getElementById(object_id);
    elem.style.visibility = 'visible';
}

function processAuthChange() {
    let authCbx = document.getElementById("pageLoginForm:login_business:authCbx");
    const pwdLnkId = "pageLoginForm:login_business:divPwdLink";
    if (authCbx != null) {
        let rsaSelected = authCbx.options[authCbx.selectedIndex].value === 'RSA';

        if (rsaSelected === true) {
            hideObject(pwdLnkId);
        } else {
            unhideObject(pwdLnkId);
        }
    }
}

function setFocus() {
    let elt = jQuery("input[name$='UserName']");
    if (elt == null) elt = jQuery("input[name$='ssoUserName']");
    if (elt == null) elt = jQuery("input[name$='PIN']");

    elt.focus();
}

function getCipher(login_business, prefix) {
    let pwd = document.getElementById('pageLoginForm:' + prefix + login_business + ':' + prefix + 'Password');
    let salt = document.getElementById('pageLoginForm:' + prefix + login_business + ':' + prefix + 'Salt');
    pwd.value = getEncrypted(pwd.value, salt.value);
}

function getCipherBySha(login_business, prefix) {
    let isChangePwdDisabled = document.getElementById('changePwdDisabled').value;
    let userName = document.getElementById('pageLoginForm:' + prefix + login_business + ':' + prefix + 'UserName');
    if (isChangePwdDisabled === "true" && userName.value !== "sysadmin") {
        getCipher(login_business, prefix);
    } else {
        let pwd = document.getElementById('pageLoginForm:' + prefix + login_business + ':' + prefix + 'Password');
        pwd.value = sjcl.hash.sha256.hash(pwd.value);
    }
}