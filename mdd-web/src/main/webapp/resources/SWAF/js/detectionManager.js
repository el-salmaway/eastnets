jQuery(document).ready(function() {
	jQuery('#auditManagerForm').on("click", "#btnDLSearch", function() {
		jQuery("#detectionResultsTable\\:assignDetectionCbx").prop("selectedIndex",0);
		jQuery("#auditManagerForm\\:homepage_business\\:Detections_list\\:Detection_details\\:Violation_details\\:duplicateDetectionButtons\\:assignDetectionCbxDuplicate").prop("selectedIndex",0);
		jQuery("#auditManagerForm\\:homepage_business\\:Detections_list\\:Detection_details\\:violationList_ResultsTable\\:assignCbx").prop("selectedIndex",0);
	});


	jQuery('#auditManagerForm').on("change", "#auditManagerForm\\:homepage_business\\:Detections_list\\:detectionResultsTable\\:assignDetectionCbx", function() {
		jQuery("#auditManagerForm\\:homepage_business\\:Detections_list\\:Detection_details\\:Violation_details\\:duplicateDetectionButtons\\:assignDetectionCbxDuplicate").val( this.value );		
	});

	jQuery('#auditManagerForm').on("change", "#auditManagerForm\\:homepage_business\\:Detections_list\\:Detection_details\\:Violation_details\\:duplicateDetectionButtons\\:assignDetectionCbxDuplicate", function() {
		jQuery("#auditManagerForm\\:homepage_business\\:Detections_list\\:detectionResultsTable\\:assignDetectionCbx").val( this.value );
	});
	
	var hideCleanDetectionAccess = function() {
		if (jQuery('#auditManagerForm\\:homepage_business\\:Detections_list\\:openedCbx').is(':checked') === true || jQuery('#auditManagerForm\\:homepage_business\\:Detections_list\\:reportedCbx').is(':checked') === true) { 
			jQuery('#auditManagerForm\\:homepage_business\\:Detections_list\\:cleanCbx').prop('disabled', true); 
			jQuery('#auditManagerForm\\:homepage_business\\:Detections_list\\:cleanCbx').prop('checked', true); 
		} 
		else { 
			jQuery('#auditManagerForm\\:homepage_business\\:Detections_list\\:cleanCbx').prop('disabled', false); 
			jQuery('#auditManagerForm\\:homepage_business\\:Detections_list\\:cleanCbx').prop('checked', true); 
		}			

		onCleanCbxChanged();
	};

	var onCleanCbxChanged = function() {
		jQuery('#auditManagerForm\\:homepage_business\\:Detections_list\\:cleanCbxHidden').val(jQuery('#auditManagerForm\\:homepage_business\\:Detections_list\\:cleanCbx').is(':checked'));
	};

	jQuery('#auditManagerForm').on("change", "#auditManagerForm\\:homepage_business\\:Detections_list\\:openedCbx", hideCleanDetectionAccess);
	jQuery('#auditManagerForm').on("change", "#auditManagerForm\\:homepage_business\\:Detections_list\\:reportedCbx", hideCleanDetectionAccess);
	jQuery('#auditManagerForm').on("change", "#auditManagerForm\\:homepage_business\\:Detections_list\\:cleanCbx", onCleanCbxChanged);

	init();
});

function init() {

	if (jQuery('#detectionResultsTable >tbody >tr >td').length > 1) {
		jQuery("#detectionResultsTable td:empty").html("&#160;");
	}

	if (showErrorPanel === 'true') {
		jQuery("html, body").animate({ scrollTop: 0 });
	}

    setActiveTransactionFlag('false');
}

function isExactMatch(tableId, classname)
{
	table = document.getElementById(tableId);

	if (table == null) return false;

	inputs = table.getElementsByTagName('input');

	for (i = 0; i < inputs.length; i++)
	{
		if (inputs[i].type == 'checkbox')
		{
			if (inputs[i].checked == true && inputs[i].className == classname)
				return true;
		}
	}

	return false;
}

function isSelectedViolationHasAlert(tableId)
{
	table = document.getElementById(tableId);
	if (table == null) return false;

	inputs = table.getElementsByTagName('input');
	for (i = 0; i < inputs.length; i++)	{
		if (inputs[i].type == 'checkbox' && inputs[i].checked == true && inputs[i].className == 'hasAlertId') {
			return true;
		}
	}

	return false;
}

