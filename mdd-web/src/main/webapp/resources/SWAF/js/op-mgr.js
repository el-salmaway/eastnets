function getCipher1() {
    var pwd = document.getElementById('operatorManagerDetailForm:detail_business:password');
    var minAllowedLength = document.getElementById('passwordMinimumLength').value;
    // alert("Length: "+minAllowedLength);
    if (!(pwd.value.length >= minAllowedLength)) {
        passwordNotMeetLengthPolicy();
    }

    var maxAllowedLength = document.getElementById('passwordMaximumLength').value;
    if (!(pwd.value.length <= maxAllowedLength)) {
        passwordNotMeetMaxLengthPolicy();
    }

    var isValidNewPassword = checkPasswordPolicy(pwd.value);
    if (!isValidNewPassword) {
        passwordNotMeetPolicy();
    }

    var hashedPass = sjcl.hash.sha256.hash(pwd.value);
    pwd.value = hashedPass;
}

function getCipher2() {
    var pwdConfirm = document.getElementById('operatorManagerDetailForm:detail_business:confPassword');
    var minAllowedLength = document.getElementById('passwordMinimumLength').value;
    if (!(pwdConfirm.value.length >= minAllowedLength)) {
        passwordNotMeetLengthPolicy();
    }
    var maxAllowedLength = document.getElementById('passwordMaximumLength').value;
    if (!(pwdConfirm.value.length <= maxAllowedLength)) {
        passwordNotMeetMaxLengthPolicy();
    }
    var hashedPass = sjcl.hash.sha256.hash(pwdConfirm.value);
    pwdConfirm.value = hashedPass;
}